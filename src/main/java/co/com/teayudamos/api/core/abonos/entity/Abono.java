package co.com.teayudamos.api.core.abonos.entity;

import co.com.teayudamos.api.core.abonos.controller.dto.AbonoDTO;
import co.com.teayudamos.api.core.cierres.entity.Cierre;
import co.com.teayudamos.api.core.prestamos.entity.Prestamo;
import co.com.teayudamos.api.generic.entity.MedioPago;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.sql.Blob;
import java.util.Date;

@Entity
@Table(name = "TAYU_ABONOS")
public class Abono {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long codigo;

    @Column(name = "NMVLR_CUOTA", nullable = false, length = 15, precision = 2)
    private double valor;

    @Column(name = "NMVLR_INTERES", nullable = false, length = 15, precision = 2)
    private double interes;

    @Column(name = "NMCUOTA", nullable = false, length = 15)
    private int cuota;

    @Column(name = "NMVLR_INTERES_PAGADO", nullable = true, length = 15, precision = 2)
    private double interesPagado;

    @Column(name = "NMVLR_CUOTA_PAGADO", nullable = true, length = 15, precision = 2)
    private double valorPagado;

    @Column(name = "BLCOMPROBANTE", nullable = true)
    private Blob comprobante;

    @Column(name = "FEABONO", nullable = true)
    private Date fechaAbono;

    @Column(name = "FECREACION", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date fechaCreacion;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ID_PRESTAMO", nullable = false)
    private Prestamo prestamo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_MEDIO_PAGO", nullable = true)
    private MedioPago medioPago;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ID_CIERRE", nullable = true)
    private Cierre cierre;

    @Column(name = "ID_USUARIO_CREACION", nullable = false, length = 15)
    private Long usuarioCreacion;

    @Column(name = "ID_USUARIO_ACTUALIZACION", nullable = false, length = 15)
    private Long usuarioActualizacion;

    @Column(name = "SNACTIVO", nullable = false, length = 1)
    private boolean snActivo;

    public Abono() {
    }

    public Abono(AbonoDTO abonoDTO) {
        if(abonoDTO != null) {
            this.codigo = abonoDTO.getCodigo();
            this.valor = abonoDTO.getValor();
            this.interes = abonoDTO.getInteres();
            this.cuota = abonoDTO.getCuota();
            this.interesPagado = abonoDTO.getInteresPagado();
            this.valorPagado = abonoDTO.getValorPagado();
            this.fechaAbono = abonoDTO.getFechaAbono();
            this.fechaCreacion = abonoDTO.getFechaCreacion();
            this.prestamo = new Prestamo(abonoDTO.getCodigoPrestamo());
            this.medioPago = new MedioPago(abonoDTO.getCodigoMedioPago());
            this.cierre = new Cierre(abonoDTO.getCodigoCierre());
            this.usuarioCreacion = abonoDTO.getUsuarioCreacion();
            this.usuarioActualizacion = abonoDTO.getUsuarioActualizacion();
            this.snActivo = abonoDTO.isSnActivo();
        }
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getInteres() {
        return interes;
    }

    public void setInteres(double interes) {
        this.interes = interes;
    }

    public int getCuota() {
        return cuota;
    }

    public void setCuota(int cuota) {
        this.cuota = cuota;
    }

    public double getInteresPagado() {
        return interesPagado;
    }

    public void setInteresPagado(double interesPagado) {
        this.interesPagado = interesPagado;
    }

    public double getValorPagado() {
        return valorPagado;
    }

    public void setValorPagado(double valorPagado) {
        this.valorPagado = valorPagado;
    }

    public Blob getComprobante() {
        return comprobante;
    }

    public void setComprobante(Blob comprobante) {
        this.comprobante = comprobante;
    }

    public Date getFechaAbono() {
        return fechaAbono;
    }

    public void setFechaAbono(Date fechaAbono) {
        this.fechaAbono = fechaAbono;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Prestamo getPrestamo() {
        return prestamo;
    }

    public void setPrestamo(Prestamo prestamo) {
        this.prestamo = prestamo;
    }

    public MedioPago getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(MedioPago medioPago) {
        this.medioPago = medioPago;
    }

    public Cierre getCierre() {
        return cierre;
    }

    public void setCierre(Cierre cierre) {
        this.cierre = cierre;
    }

    public Long getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(Long usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Long getUsuarioActualizacion() {
        return usuarioActualizacion;
    }

    public void setUsuarioActualizacion(Long usuarioActualizacion) {
        this.usuarioActualizacion = usuarioActualizacion;
    }

    public boolean isSnActivo() {
        return snActivo;
    }

    public void setSnActivo(boolean snActivo) {
        this.snActivo = snActivo;
    }
}