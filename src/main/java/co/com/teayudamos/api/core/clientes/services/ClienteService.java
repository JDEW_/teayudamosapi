package co.com.teayudamos.api.core.clientes.services;

import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.core.prestamos.services.PrestamoService;
import co.com.teayudamos.api.exception.ServiceException;
import co.com.teayudamos.api.generic.entity.TiposDoc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import co.com.teayudamos.api.core.clientes.controller.dto.ClienteDTO;
import co.com.teayudamos.api.core.socios.entity.Socio;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ClienteService {

    @Autowired
    private IClienteRepository clienteRepository;

    @Autowired
    private PrestamoService prestamoService;

    public void save(Cliente cliente){
        if(cliente.getCodigo() == null || cliente.getCodigo() == 0){
            cliente.setSnActivo(true);
            cliente.setFechaCreaion(new Date());

        }
        cliente.setFechaCreaion(new Date());
        cliente.setFechaModificacion(new Date());
        clienteRepository.save(cliente);
    }

    public void deleteById(Long id) throws ServiceException {

        Cliente cliente = (new Cliente(id));

        Long countReferidos = clienteRepository.countByCodeudor(cliente);
        if(countReferidos == null || countReferidos.longValue() == 0){
            Long countPrestamos = prestamoService.countByCliente(cliente);

            if(countReferidos == null || countReferidos.longValue() == 0) {
                clienteRepository.deleteById(id);
            }else{
                throw new ServiceException("El cliente no se puede eliminar ya que que es un cliente que tiene o tuvo algun prestamo", 3);
            }
        }else{
            throw new ServiceException("El cliente no se puede eliminar ya que que es condeudor de otro cliete", 2);
        }
    }


    public Cliente getById(Long id){
        return clienteRepository.getOne(id);
    }

    public Cliente findByTiposDocAndCedula(TiposDoc tiposDoc, String cedula){
        return clienteRepository.findByTiposDocAndCedula(tiposDoc, cedula);
    }

    public List<Cliente> findAll(){
        return clienteRepository.findAll();
    }

    public List<ClienteDTO> getAllCLientesDTO(List<Cliente> clientes) {
        List<ClienteDTO> clientesDTO = new ArrayList<>();
        for (Cliente cliente : clientes) {
            ClienteDTO clienteDTO = new ClienteDTO(cliente);
            clientesDTO.add(clienteDTO);
        }
        return clientesDTO;
    }
}