package co.com.teayudamos.api.core.reportes.report;

public class Cartera {

    private String cliente;
    private double valorRestante;
    private double valorCuota;
    private String telefonos;
    private String fechaUltimoAbono;
    private String nombreReferencia;
    private String telefonoReferencia;
    private double idPrestamo;
    private double valorPrestamo;

    public Cartera() {
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public double getValorRestante() {
        return valorRestante;
    }

    public void setValorRestante(double valorRestante) {
        this.valorRestante = valorRestante;
    }

    public double getValorCuota() {
        return valorCuota;
    }

    public void setValorCuota(double valorCuota) {
        this.valorCuota = valorCuota;
    }

    public String getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(String telefonos) {
        this.telefonos = telefonos;
    }

    public String getFechaUltimoAbono() {
        return fechaUltimoAbono;
    }

    public void setFechaUltimoAbono(String fechaUltimoAbono) {
        this.fechaUltimoAbono = fechaUltimoAbono;
    }

    public String getNombreReferencia() {
        return nombreReferencia;
    }

    public void setNombreReferencia(String nombreReferencia) {
        this.nombreReferencia = nombreReferencia;
    }

    public String getTelefonoReferencia() {
        return telefonoReferencia;
    }

    public void setTelefonoReferencia(String telefonoReferencia) {
        this.telefonoReferencia = telefonoReferencia;
    }

    public double getIdPrestamo() {
        return idPrestamo;
    }

    public void setIdPrestamo(double idPrestamo) {
        this.idPrestamo = idPrestamo;
    }

    public double getValorPrestamo() {
        return valorPrestamo;
    }

    public void setValorPrestamo(double valorPrestamo) {
        this.valorPrestamo = valorPrestamo;
    }
}