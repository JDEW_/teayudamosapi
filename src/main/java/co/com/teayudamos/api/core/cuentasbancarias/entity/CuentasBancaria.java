package co.com.teayudamos.api.core.cuentasbancarias.entity;

import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.core.cuentasbancarias.controller.dto.CuentaBancariaDTO;
import co.com.teayudamos.api.core.socios.entity.Socio;
import co.com.teayudamos.api.generic.entity.Banco;
import co.com.teayudamos.api.generic.entity.TiposDoc;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TAYU_CUENTAS_BANCARIAS")
public class CuentasBancaria implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long codigo;

    @Column(name = "DSCUENTA")
    private String dsCuenta;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ID_CLIENTE", nullable=false)
    private Cliente cliente;

    @Column(name = "DSTIPO_CUENTA", nullable = false, length = 1)
    private String tipoCuenta;

    @Column(name = "SNCUENTA_PRINCIPAL", nullable = false, length = 1)
    private String snCuentaPrincipal;

    @Column(name = "SNACTIVO", nullable = false, length = 1)
    private boolean snActivo;

    @Column(name = "ID_USUARIO_CREACION", nullable = false, length = 15)
    private Long usuarioCreaciom;

    @Column(name = "FECREACION", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date fechaCreaion;

    @Column(name = "FEMODIFICACION", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date fechaModificacion;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_BANCO", nullable = false)
    private Banco banco;

    public CuentasBancaria() {
    }

    public CuentasBancaria(Long codigo) {
        this.codigo = codigo;
    }

    public CuentasBancaria(CuentaBancariaDTO cuentaBancariaDTO) {
        this.codigo = cuentaBancariaDTO.getCodigo();
        this.dsCuenta = cuentaBancariaDTO.getDsCuenta();
        this.tipoCuenta = cuentaBancariaDTO.getTipoCuenta();
        this.snCuentaPrincipal = cuentaBancariaDTO.getSnCuentaPrincipal();
        this.snActivo = cuentaBancariaDTO.isSnActivo();
        this.cliente = new Cliente(cuentaBancariaDTO.getCodigoCliente());
        this.banco = new Banco(cuentaBancariaDTO.getCodigoBanco());
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getDsCuenta() {
        return dsCuenta;
    }

    public void setDsCuenta(String dsCuenta) {
        this.dsCuenta = dsCuenta;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public String getSnCuentaPrincipal() {
        return (snCuentaPrincipal == null || snCuentaPrincipal.equals("") ? "N" : snCuentaPrincipal);
    }

    public void setSnCuentaPrincipal(String snCuentaPrincipal) {
        this.snCuentaPrincipal = snCuentaPrincipal;
    }

    public boolean isSnActivo() {
        return snActivo;
    }

    public void setSnActivo(boolean snActivo) {
        this.snActivo = snActivo;
    }

    public Long getUsuarioCreaciom() {
        return usuarioCreaciom;
    }

    public void setUsuarioCreaciom(Long usuarioCreaciom) {
        this.usuarioCreaciom = usuarioCreaciom;
    }

    public Date getFechaCreaion() {
        return fechaCreaion;
    }

    public void setFechaCreaion(Date fechaCreaion) {
        this.fechaCreaion = fechaCreaion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }
}
