package co.com.teayudamos.api.core.socios.services;

import co.com.teayudamos.api.core.socios.controller.dto.SocioDTO;
import co.com.teayudamos.api.core.socios.entity.Socio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SocioService {

    @Autowired
    private ISocioRepository socioRepository;

    public void save(Socio socio){
        socioRepository.save(socio);
    }

    public void update(Socio socio){
        socioRepository.saveAndFlush(socio);
    }

    public void deleteById(Long id){
        socioRepository.deleteById(id);
    }

    public Socio getById(Long id){
        return socioRepository.getOne(id);
    }

    public List<Socio> findAll(){
        return socioRepository.findAll();
    }

    public List<SocioDTO> getAllSociosDTO(List<Socio> socios) {
        List<SocioDTO> sociosDTO = new ArrayList<>();
        for (Socio socio : socios) {
            SocioDTO socioDTO = new SocioDTO(socio);
            sociosDTO.add(socioDTO);
        }
        return sociosDTO;
    }
}
