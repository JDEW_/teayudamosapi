package co.com.teayudamos.api.core.prestamos.type;

import java.util.ArrayList;
import java.util.List;

public enum Estado {
    ACTIVO(0, "ACTIVO"),
    PAGADO(1, "PAGADO"),
    ELIMINADO(2, "ELIMINADO");

    private int codigo;
    private String nombre;

    Estado(int codigo, String nombre) {
        this.codigo =codigo;
        this.nombre = nombre;
    }

    public static Estado getById(int key){
        for (Estado estado: values()) {
            if (key == estado.getCodigo()){
                return estado;
            }

        }
        return null;
    }

    public static List<Estado> getActivos(){
        List<Estado> estados = new ArrayList<>();
        estados.add(ACTIVO);
        estados.add(PAGADO);
        return estados;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}