package co.com.teayudamos.api.core.cuentasbancarias.controller;


import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.core.cuentasbancarias.controller.dto.CuentaBancariaDTO;
import co.com.teayudamos.api.core.cuentasbancarias.entity.CuentasBancaria;
import co.com.teayudamos.api.core.cuentasbancarias.services.CuentaBancariaService;
import co.com.teayudamos.api.exception.ServiceException;
import co.com.teayudamos.api.security.dto.ICurrentUser;
import co.com.teayudamos.api.security.dto.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = {"http://localhost:4200", "https://teayudamos-99.firebaseapp.com", "https://teayudamos-all.firebaseapp.com"})
@RequestMapping("/cuentasbancarias/v1/")
public class CuentaBancariaController {

    @Autowired
    private CuentaBancariaService cuentaBancariaService;


    @GetMapping("/findByCuentas/{idCliente}")
    public ResponseEntity<List<CuentaBancariaDTO>> findByCuentas(@PathVariable String idCliente) {
        List<CuentasBancaria> cuentasBancarias = cuentaBancariaService.findAllByCliente(new Cliente(Long.parseLong(idCliente)));
        List<CuentaBancariaDTO> cuentas = cuentaBancariaService.getAllCuentasBancariaDTO(cuentasBancarias);
        return ResponseEntity.ok(cuentas);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CuentaBancariaDTO> save(@ICurrentUser UserPrincipal currentUser, @RequestBody CuentaBancariaDTO cuentaBancariaDTO) throws ServiceException {

        CuentasBancaria cuentasBancaria =  new CuentasBancaria(cuentaBancariaDTO);

        cuentasBancaria.setUsuarioCreaciom(new Long(1));

        cuentaBancariaService.save(cuentasBancaria);
        return ResponseEntity.ok(cuentaBancariaDTO);
    }


    @DeleteMapping("/deleteById/{idCuenta}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CuentaBancariaDTO> deleteByCodigoAndCliente(@PathVariable String idCuenta) {
        cuentaBancariaService.deleteById(Long.parseLong(idCuenta));
        return ResponseEntity.ok(new CuentaBancariaDTO(idCuenta));
    }


}
