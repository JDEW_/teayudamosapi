package co.com.teayudamos.api.core.reportes.services;

import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.core.prestamos.services.PrestamoService;
import co.com.teayudamos.api.core.clientes.services.ClienteService;
import co.com.teayudamos.api.core.reportes.report.Cartera;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class CarteraServices {

    @Autowired
    private PrestamoService prestamoService;

    @Autowired
    private ClienteService clienteService;

    private static final String NO_REGISTRA = "No registra";

    public List<Cartera> generateReportCartera() {
        List<Cartera> list = new ArrayList<>();

        Object[][] prestamosPendientesPago = prestamoService.getPrestamosPendientesPago();
        for (Object[] data : prestamosPendientesPago) {

            BigInteger idCliente = (BigInteger) data[0];
            Double valorCuota = (Double) data[1];
            String telefono = (String) data[2];
            String nombreReferencia = (String) data[3];
            String telefonoReferencia = (String) data[4];
            BigInteger idPrestamo = (BigInteger) data[5];
            Double valorPrestamo = (Double) data[6];
            Double valorRestante = (Double) data[7];


            Cartera cartera = new Cartera();
            Cliente cliente = clienteService.getById(idCliente.longValue());

            cartera.setCliente(cliente.getNombre());
            cartera.setValorCuota(valorCuota != null ? valorCuota : 0D);
            cartera.setTelefonos(telefono != null ? telefono : NO_REGISTRA);
            cartera.setNombreReferencia(nombreReferencia != null ? nombreReferencia : NO_REGISTRA);
            cartera.setTelefonoReferencia(telefonoReferencia != null ? telefonoReferencia : NO_REGISTRA);
            cartera.setIdPrestamo(idPrestamo != null ? idPrestamo.doubleValue() : 0D);
            cartera.setValorPrestamo(valorPrestamo != null ? valorPrestamo : 0D);
            cartera.setValorRestante(valorRestante != null ? valorRestante: 0D);

            list.add(cartera);
        }
        return list;
    }
}