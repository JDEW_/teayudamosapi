package co.com.teayudamos.api.core.abonos.controller;


import co.com.teayudamos.api.core.abonos.controller.dto.AbonoDTO;
import co.com.teayudamos.api.core.abonos.entity.Abono;
import co.com.teayudamos.api.core.abonos.services.AbonoService;
import co.com.teayudamos.api.core.prestamos.controller.dto.PrestamoDTO;
import co.com.teayudamos.api.core.prestamos.entity.Prestamo;
import co.com.teayudamos.api.exception.ServiceException;
import co.com.teayudamos.api.security.dto.ICurrentUser;
import co.com.teayudamos.api.security.dto.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = {"http://localhost:4200", "https://teayudamos-99.firebaseapp.com", "https://teayudamos-all.firebaseapp.com"})
@RequestMapping("/abonos/v1/")
public class AbonoController {

    @Autowired
    private AbonoService abonoService;

    @GetMapping("/getByPrestamo/{idPrestamo}")
    public ResponseEntity<List<AbonoDTO>> getByPrestamo(@PathVariable String idPrestamo) {
        List<Abono> abonos = abonoService.findByPrestamo(new Prestamo(Long.parseLong(idPrestamo)));
        List<AbonoDTO> prestamosDTO = abonoService.getAllAbonosDTO(abonos);
        return ResponseEntity.ok(prestamosDTO);
    }

    @GetMapping("/getAbonoDefault/{idPrestamo}")
    public ResponseEntity<AbonoDTO> getAbonoDefault(@PathVariable Long idPrestamo) {
        AbonoDTO abonoDTO = abonoService.getAbonoDefault(idPrestamo);
        return ResponseEntity.ok(abonoDTO);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<AbonoDTO> save(@ICurrentUser UserPrincipal currentUser, @RequestBody AbonoDTO abonoDTO) {
        Abono abono = new Abono(abonoDTO);
        abono.setUsuarioActualizacion(new Long(1));
        abono.setUsuarioCreacion(new Long(1));

        abonoService.create(abono);

        return ResponseEntity.ok(abonoDTO);
    }


    @PutMapping("/updateInactiveState")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<AbonoDTO>  updateInactiveState(@ICurrentUser UserPrincipal currentUser, @RequestBody AbonoDTO abonoDTO) {
        Abono abono = new Abono(abonoDTO);
        abono.setUsuarioActualizacion(new Long(1));
        abono.setUsuarioCreacion(new Long(1));

        abonoService.updateInactiveState(abono);

        return ResponseEntity.ok(abonoDTO);
    }
}
