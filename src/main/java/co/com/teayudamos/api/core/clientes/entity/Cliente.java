package co.com.teayudamos.api.core.clientes.entity;

import co.com.teayudamos.api.core.clientes.controller.dto.ClienteDTO;
import co.com.teayudamos.api.core.cuentasbancarias.entity.CuentasBancaria;
import co.com.teayudamos.api.core.socios.entity.Socio;
import co.com.teayudamos.api.generic.entity.TiposDoc;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "TAYU_CLIENTES")
public class Cliente implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public Cliente(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long codigo;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_CDTIPO_DOC", nullable = false)
    private TiposDoc tiposDoc;

    @Column(name = "DSCEDULA", nullable = false, length = 20)
    private String cedula;

    @Column(name = "DSNOMBRE", nullable = false, length = 200)
    private String nombre;

    @Column(name = "DSDIRECCION", nullable = false, length = 200)
    private String direccion;

    @Column(name = "DSEMAIL", nullable = false, length = 200)
    private String email;

    @Column(name = "DSTELEFONO", nullable = true, length = 7)
    private String telefono;

    @Column(name = "DSCELULAR", nullable = false, length = 10)
    private String celular;

    @Column(name = "SNACTIVO", nullable = false, length = 1)
    private boolean snActivo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_CODEUDOR", nullable = true)
    private Cliente codeudor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_SOCIO", nullable = false)
    private Socio socio;

    @Column(name = "FECREACION", nullable = false)
    @CreatedDate
    private Date fechaCreaion;

    @Column(name = "FEMODIFICACION", nullable = false)
    @LastModifiedDate
    private Date fechaModificacion;

    @OneToMany(mappedBy = "cliente", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<CuentasBancaria> cuentasBancarias;

    @Column(name = "DSNOMBRE_REF", nullable = false, length = 100)
    private String nombreReferencia;

    @Column(name = "DSTEL_RER", nullable = false, length = 10)
    private String telReferencia;

    public Cliente(ClienteDTO clienteDTO){
        this.codigo = clienteDTO.getCodigo();
        this.tiposDoc = new TiposDoc(clienteDTO.getTipoDoc());
        this.cedula = clienteDTO.getCedula();
        this.celular = clienteDTO.getCelular();
        this.nombre = clienteDTO.getNombre();
        this.direccion = clienteDTO.getDireccion();
        this.email = clienteDTO.getEmail();
        this.telefono = clienteDTO.getTelefono();
        this.snActivo = clienteDTO.isSnActivo();
        this.codeudor = (clienteDTO.getReferidoId() != null ? new Cliente(clienteDTO.getReferidoId()) : null);
        this.socio = new Socio(clienteDTO.getSocioId());
        this.nombreReferencia = clienteDTO.getNombreReferencia();
        this.telReferencia = clienteDTO.getTelReferencia();
    }

    public Cliente(long codigo){
        this.codigo = codigo;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public TiposDoc getTiposDoc() {
        return tiposDoc;
    }

    public void setTiposDoc(TiposDoc tiposDoc) {
        this.tiposDoc = tiposDoc;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public boolean isSnActivo() {
        return snActivo;
    }

    public void setSnActivo(boolean snActivo) {
        this.snActivo = snActivo;
    }

    public Cliente getCodeudor() {
        return codeudor;
    }

    public void setCodeudor(Cliente codeudor) {
        this.codeudor = codeudor;
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    public Date getFechaCreaion() {
        return fechaCreaion;
    }

    public void setFechaCreaion(Date fechaCreaion) {
        this.fechaCreaion = fechaCreaion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public List<CuentasBancaria> getCuentasBancarias() {
        return cuentasBancarias;
    }

    public void setCuentasBancarias(List<CuentasBancaria> cuentasBancarias) {
        this.cuentasBancarias = cuentasBancarias;
    }

    public String getNombreReferencia() {
        return nombreReferencia;
    }

    public void setNombreReferencia(String nombreReferencia) {
        this.nombreReferencia = nombreReferencia;
    }

    public String getTelReferencia() {
        return telReferencia;
    }

    public void setTelReferencia(String telReferencia) {
        this.telReferencia = telReferencia;
    }
}
