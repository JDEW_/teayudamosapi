package co.com.teayudamos.api.core.prestamos.services;

import co.com.teayudamos.api.core.abonos.services.AbonoService;
import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.core.prestamos.entity.Prestamo;
import co.com.teayudamos.api.core.prestamos.type.Estado;
import co.com.teayudamos.api.exception.ServiceException;
import co.com.teayudamos.api.generic.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import co.com.teayudamos.api.core.prestamos.controller.dto.PrestamoDTO;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class PrestamoService {

    @Autowired
    private IPrestamoRepository prestamoRepository;

    @Autowired
    private AbonoService abonoService;

    public void save(Prestamo prestamo) {

        if(prestamo.getCodigo() != null && prestamo.getCodigo() > 0){
            prestamo.setFechaActualizacion(new Date());

            prestamo.setFeMaxPago(Utils.sumarDiasAFecha(prestamo.getFePago(), 5));
        }else{
            prestamo.setFechaCreacion(new Date());
        }

        prestamoRepository.save(prestamo);
    }

    public void deleteById(Long id) throws ServiceException {

        Long count = abonoService.countByPrestamoAndSnActivo(new Prestamo(id));

        if(count != null && count == 0) {
            Prestamo prestamo = getById(id);

            prestamo.setEstado(Estado.ELIMINADO);

            save(prestamo);
        }else{
            throw new ServiceException("Señor usuario no se puede eliminar el prestamo ya que tiene asociado abonos, elimine los abonos e intente nuevamente.", 1);
        }
    }

    public Prestamo getById(Long id) {
        return prestamoRepository.getOne(id);
    }

    public List<Prestamo> findAll() {
        return prestamoRepository.findAllByEstadoIn(Estado.getActivos());
    }

    public Long countByCliente(Cliente cliente){
        return prestamoRepository.countByCliente(cliente);
    }

    public List<PrestamoDTO> getAllPrestamosDTO(List<Prestamo> prestamos) {
        List<PrestamoDTO> prestamosDTO = new ArrayList<>();
        for (Prestamo prestamo : prestamos) {
            PrestamoDTO prestamoDTO = new PrestamoDTO(prestamo);
            prestamosDTO.add(prestamoDTO);
        }
        return prestamosDTO;
    }

    public Object[][] getBalanceByYear(String year){
        return prestamoRepository.getBalanceByYear(year);
    }

    public Object[][] getPrestamosPendientesPago (){
        return prestamoRepository.getPrestamosPendientesPago();
    }
}