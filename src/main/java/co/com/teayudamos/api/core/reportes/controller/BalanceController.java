package co.com.teayudamos.api.core.reportes.controller;

import co.com.teayudamos.api.core.reportes.report.Balance;
import co.com.teayudamos.api.core.reportes.services.BalanceServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200", "https://teayudamos-99.firebaseapp.com", "https://teayudamos-all.firebaseapp.com"})
@RestController
@RequestMapping("/balance/v1/")
public class BalanceController {

    @Autowired
    private BalanceServices balanceServices;

    @GetMapping("/findBalance/{year}")
    public ResponseEntity<List<Balance>> findBalance(@PathVariable String year) {

        List<Balance> balanceReport = balanceServices.generateReportBalance(year);

        return ResponseEntity.ok(balanceReport);
    }
}
