package co.com.teayudamos.api.core.cuentasbancarias.controller.dto;

import co.com.teayudamos.api.core.cuentasbancarias.entity.CuentasBancaria;

public class CuentaBancariaDTO {


    private long codigo;
    private String tipoCuenta;
    private String dsTipoCuenta;
    private String dsCuenta;
    private String snCuentaPrincipal;
    private boolean snActivo;
    private long codigoCliente;
    private String nombreCliente;
    private long codigoBanco;
    private String nombreBanco;

    public CuentaBancariaDTO(CuentasBancaria cuentasBancaria) {
        this.codigo = cuentasBancaria.getCodigo();
        this.dsCuenta = cuentasBancaria.getDsCuenta();
        this.tipoCuenta = cuentasBancaria.getTipoCuenta();
        this.dsTipoCuenta = (cuentasBancaria.getTipoCuenta() != null && "1".equals(cuentasBancaria.getTipoCuenta()) ? "AHORROS" : "CORRIENTE");
        this.snCuentaPrincipal = cuentasBancaria.getSnCuentaPrincipal();
        this.snActivo = cuentasBancaria.isSnActivo();
        this.codigoCliente = cuentasBancaria.getCliente().getCodigo();
        this.nombreCliente = cuentasBancaria.getCliente().getNombre();
        this.codigoBanco = cuentasBancaria.getBanco().getCodigo();
        this.nombreBanco = cuentasBancaria.getBanco().getNombre();
    }

    public CuentaBancariaDTO(String dsCuenta) {
        this.dsCuenta = dsCuenta;
    }

    public CuentaBancariaDTO() {

    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getTipoCuenta() {
        return tipoCuenta;
    }

    public void setTipoCuenta(String tipoCuenta) {
        this.tipoCuenta = tipoCuenta;
    }

    public String getSnCuentaPrincipal() {
        return snCuentaPrincipal;
    }

    public void setSnCuentaPrincipal(String snCuentaPrincipal) {
        this.snCuentaPrincipal = snCuentaPrincipal;
    }

    public boolean isSnActivo() {
        return snActivo;
    }

    public void setSnActivo(boolean snActivo) {
        this.snActivo = snActivo;
    }

    public long getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(long codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public Long getCodigoBanco() {
        return codigoBanco;
    }

    public void setCodigoBanco(Long codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getDsCuenta() {
        return dsCuenta;
    }

    public void setDsCuenta(String dsCuenta) {
        this.dsCuenta = dsCuenta;
    }

    public void setCodigoBanco(long codigoBanco) {
        this.codigoBanco = codigoBanco;
    }

    public String getNombreBanco() {
        return nombreBanco;
    }

    public void setNombreBanco(String nombreBanco) {
        this.nombreBanco = nombreBanco;
    }

    public String getDsTipoCuenta() {
        return dsTipoCuenta;
    }

    public void setDsTipoCuenta(String dsTipoCuenta) {
        this.dsTipoCuenta = dsTipoCuenta;
    }
}
