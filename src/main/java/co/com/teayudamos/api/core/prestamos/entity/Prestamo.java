package co.com.teayudamos.api.core.prestamos.entity;

import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.core.prestamos.controller.dto.PrestamoDTO;
import co.com.teayudamos.api.core.prestamos.type.Estado;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.sql.Blob;
import java.util.Date;

@Entity
@Table(name = "TAYU_PRESTAMOS")
public class Prestamo {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long codigo;

    @Column(name = "NMVALOR", nullable = false, length = 15, precision = 2)
    private double valor;

    @Column(name = "NMVALOR_INTERES", nullable = false, length = 15, precision = 2)
    private double valorCuotaInteres;

    @Column(name = "NMVALOR_CUOTA", nullable = false, length = 15, precision = 2)
    private double valorCuota;

    @Column(name = "NMCUOTAS", nullable = false, length = 2)
    private int cuotas;

    @Column(name = "BLCONTRATO", nullable = true)
    private Blob contrato;

    @Enumerated(EnumType.ORDINAL)
    @Column(name = "CDESTADO", nullable = false, length = 2)
    private Estado estado;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ID_CLIENTE", nullable=false)
    private Cliente cliente;

    @Column(name = "ID_USUARIO_CREACION", nullable = false, length = 15)
    private Long usuarioCreaciom;

    @Column(name = "FECREACION", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date fechaCreacion;

    @Column(name = "FEENTREGA", nullable = true)
    private Date fechaEntrega;

    @Column(name = "FEACTUALIZACION", nullable = false)
    private Date fechaActualizacion;

    @Column(name = "FEPAGO", nullable = false)
    private Date fePago;

    @Column(name = "FEMAXPAGO", nullable = false)
    private Date feMaxPago;

    public Prestamo() {
    }

    public Prestamo(Long codigo) {
        this.codigo = codigo;
    }

    public Prestamo(PrestamoDTO prestamoDTO) {
        this.codigo = prestamoDTO.getCodigo();
        this.valor = prestamoDTO.getValor();
        this.cuotas = prestamoDTO.getCuotas();
        this.estado = Estado.getById(prestamoDTO.getEstado());
        this.fechaCreacion = prestamoDTO.getFeCreacion();
        this.fechaEntrega = prestamoDTO.getFeEntrega();
        this.fechaActualizacion = new Date();
        this.fePago = prestamoDTO.getFePago();
        this.feMaxPago = prestamoDTO.getFeMaxPago();
        this.cliente = new Cliente(prestamoDTO.getIdCliente());
        this.valorCuota = prestamoDTO.getValorCuota();
        this.valorCuotaInteres = prestamoDTO.getValorCuotaInteres();
    }


    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public int getCuotas() {
        return cuotas;
    }

    public void setCuotas(int cuotas) {
        this.cuotas = cuotas;
    }

    public Blob getContrato() {
        return contrato;
    }

    public void setContrato(Blob contrato) {
        this.contrato = contrato;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Long getUsuarioCreaciom() {
        return usuarioCreaciom;
    }

    public void setUsuarioCreaciom(Long usuarioCreaciom) {
        this.usuarioCreaciom = usuarioCreaciom;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Date getFePago() {
        return fePago;
    }

    public void setFePago(Date fePago) {
        this.fePago = fePago;
    }

    public Date getFeMaxPago() {
        return feMaxPago;
    }

    public void setFeMaxPago(Date feMaxPago) {
        this.feMaxPago = feMaxPago;
    }

    public double getValorCuotaInteres() {
        return valorCuotaInteres;
    }

    public void setValorCuotaInteres(double valorCuotaInteres) {
        this.valorCuotaInteres = valorCuotaInteres;
    }

    public double getValorCuota() {
        return valorCuota;
    }

    public void setValorCuota(double valorCuota) {
        this.valorCuota = valorCuota;
    }
}
