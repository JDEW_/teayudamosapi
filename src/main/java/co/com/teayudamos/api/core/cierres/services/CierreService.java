package co.com.teayudamos.api.core.cierres.services;

import co.com.teayudamos.api.core.cierres.entity.Cierre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CierreService {

    @Autowired
    private ICierreRepository cierreRepository;

    public void save(Cierre cierre){
        cierreRepository.save(cierre);
    }

    public void update(Cierre cierre){
        cierreRepository.saveAndFlush(cierre);
    }

    public Cierre getCierreByFechaCierreIsNull(){
        return cierreRepository.getCierreByFechaCierreIsNull();
    }

}
