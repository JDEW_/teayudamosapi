package co.com.teayudamos.api.core.cuentasbancarias.services;

import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.core.cuentasbancarias.entity.CuentasBancaria;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface ICuentaBancariaRepository extends JpaRepository<CuentasBancaria, Long> {

    List<CuentasBancaria> findAllByCliente(Cliente cliente);
    Boolean existsByDsCuentaAndCliente(String dsCuenta, Cliente cliente);

    @Modifying
    @Transactional
    @Query("delete from CuentasBancaria u where u.codigo = ?1")
    void deleteById(Long codigo);



}
