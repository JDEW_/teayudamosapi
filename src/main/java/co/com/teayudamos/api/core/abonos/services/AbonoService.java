package co.com.teayudamos.api.core.abonos.services;

import co.com.teayudamos.api.core.abonos.controller.dto.AbonoDTO;
import co.com.teayudamos.api.core.abonos.entity.Abono;
import co.com.teayudamos.api.core.cierres.entity.Cierre;
import co.com.teayudamos.api.core.cierres.services.CierreService;
import co.com.teayudamos.api.core.prestamos.entity.Prestamo;
import co.com.teayudamos.api.core.prestamos.services.PrestamoService;
import co.com.teayudamos.api.core.prestamos.type.Estado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional
public class AbonoService {

    @Autowired
    private IAbonoRepository abonoRepository;

    @Autowired
    private PrestamoService prestamoService;

    @Autowired
    private CierreService cierreService;

    @Transactional
    public void save(Abono abono){
        Cierre cierre = cierreService.getCierreByFechaCierreIsNull();
        abono.setCierre(cierre);

        abono.setFechaCreacion(new Date());

        abonoRepository.save(abono);

        Prestamo prestamo = prestamoService.getById(abono.getPrestamo().getCodigo());

        Double totalAbono = abonoRepository.sumAbonosByPrestamo(prestamo);
        if(totalAbono != null && prestamo.getValor() == totalAbono){
            prestamo.setEstado(Estado.PAGADO);
            prestamoService.save(prestamo);
        }

    }

    public void create (Abono abono){
        abono.setSnActivo(true);
        save(abono);
    }


    public void updateInactiveState (Abono abono){
        abono.setSnActivo(false);
        save(abono);
    }

    @Transactional
    public AbonoDTO getAbonoDefault(Long idPrestamo){

        Prestamo prestamo = prestamoService.getById(idPrestamo);
        AbonoDTO abonoDto = new AbonoDTO();
        abonoDto.setCodigoPrestamo(prestamo.getCodigo());
        abonoDto.setTotalPrestamo(prestamo.getValor());
        abonoDto.setNombreCliente(prestamo.getCliente().getNombre());
        Double totalAbono = abonoRepository.sumAbonosByPrestamo(prestamo);
        abonoDto.setTotalAbonado(totalAbono != null ? totalAbono.doubleValue() : 0);
        abonoDto.setTotalRestante((prestamo.getValor()) - abonoDto.getTotalAbonado());
        abonoDto.setVlrCuotaSugerida(prestamo.getValorCuota());

        List<Abono> abonos = abonoRepository.getLastAbono(prestamo);

        int cuota = 0;
        if(abonos != null && !abonos.isEmpty()){

            for (Abono lastAbono : abonos) {
                cuota = lastAbono.getCuota();
            }
        }
        abonoDto.setValor(abonoDto.getVlrCuotaSugerida());
        abonoDto.setCuota(cuota + 1);



        return abonoDto;

    }

    public List<Abono> findByPrestamo(Prestamo prestamo){
        return abonoRepository.findByPrestamoAndSnActivo(prestamo, true);
    }

    public Long countByPrestamoAndSnActivo(Prestamo prestamo){
        return abonoRepository.countByPrestamoAndSnActivo(prestamo, true);
    }

    public List<AbonoDTO> getAllAbonosDTO(List<Abono> listAbonos){
        List<AbonoDTO> abonos = new ArrayList<>();
        for (Abono abono : listAbonos) {
            AbonoDTO abonoDTO = new AbonoDTO(abono);
            abonos.add(abonoDTO);
        }
        return abonos;
    }

    public Object[][] getBalanceByYear(String year){
        return abonoRepository.getBalanceByYear(year);
    }

    public Object[][] getBalanceByCierre(Long idCierre){
        return abonoRepository.getBalanceByCierre(idCierre);
    }



}
