package co.com.teayudamos.api.core.prestamos.controller;

import co.com.teayudamos.api.core.prestamos.controller.dto.PrestamoDTO;
import co.com.teayudamos.api.core.prestamos.services.PrestamoService;
import co.com.teayudamos.api.exception.ServiceException;
import co.com.teayudamos.api.security.dto.ICurrentUser;
import co.com.teayudamos.api.security.dto.UserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import co.com.teayudamos.api.core.prestamos.entity.Prestamo;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200", "https://teayudamos-99.firebaseapp.com", "https://teayudamos-all.firebaseapp.com"})
@RestController
@RequestMapping("/prestamos/v1/")
public class PrestamoController {

    @Autowired
    private PrestamoService prestamoService;

    @GetMapping("/findAll")
    public ResponseEntity<List<PrestamoDTO>> findAll() {
        List<Prestamo> prestamos = prestamoService.findAll();
        List<PrestamoDTO> prestamosDTO = prestamoService.getAllPrestamosDTO(prestamos);
        return ResponseEntity.ok(prestamosDTO);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<PrestamoDTO> save(@ICurrentUser UserPrincipal currentUser, @RequestBody PrestamoDTO prestamoDTO) {
        Prestamo prestamo = new Prestamo(prestamoDTO);
        prestamo.setUsuarioCreaciom(new Long(1));

        prestamoService.save(prestamo);
        return ResponseEntity.ok(prestamoDTO);
    }

    @PutMapping("/update")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<PrestamoDTO>  update(@ICurrentUser UserPrincipal currentUser, @RequestBody PrestamoDTO prestamoDTO) {
        Prestamo prestamo = new Prestamo(prestamoDTO);
        prestamo.setUsuarioCreaciom(new Long(1));

        prestamoService.save(prestamo);
        return ResponseEntity.ok(prestamoDTO);
    }

    @DeleteMapping("/delete/{idPrestamo}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<PrestamoDTO>  delete(@PathVariable String idPrestamo) throws ServiceException {
        prestamoService.deleteById(Long.parseLong(idPrestamo));
        return ResponseEntity.ok(new PrestamoDTO(Long.parseLong(idPrestamo)));
    }
}