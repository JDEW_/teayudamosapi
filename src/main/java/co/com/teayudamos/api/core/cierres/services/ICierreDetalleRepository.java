package co.com.teayudamos.api.core.cierres.services;

import co.com.teayudamos.api.core.cierres.entity.Cierre;
import co.com.teayudamos.api.core.cierres.entity.CierreDetalle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICierreDetalleRepository extends JpaRepository<CierreDetalle, Long> {

    void deleteAllByCierre(Cierre cierre);

}
