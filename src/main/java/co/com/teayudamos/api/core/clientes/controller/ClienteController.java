package co.com.teayudamos.api.core.clientes.controller;

import java.util.List;

import co.com.teayudamos.api.core.clientes.controller.dto.ClienteDTO;
import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.core.clientes.services.ClienteService;
import co.com.teayudamos.api.exception.ServiceException;
import co.com.teayudamos.api.generic.entity.TiposDoc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = {"http://localhost:4200", "https://teayudamos-99.firebaseapp.com", "https://teayudamos-all.firebaseapp.com"})
@RestController
@RequestMapping("/clientes/v1/")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @GetMapping("/findAll")
    public ResponseEntity<List<ClienteDTO>>  findAll() {
        List<Cliente> clientes = clienteService.findAll();
        List<ClienteDTO> clientesDTO = clienteService.getAllCLientesDTO(clientes);
        return ResponseEntity.ok(clientesDTO);
    }

    @GetMapping(" {codigo}")
    public ResponseEntity<ClienteDTO> getById(@PathVariable long codigo) {
        Cliente cliente = clienteService.getById(codigo);
        ClienteDTO clienteDTO = new ClienteDTO(cliente);
        return ResponseEntity.ok(clienteDTO);
    }

    @GetMapping("/getByIdentificacion/{tipoDoc}/{cedula}")
    public ResponseEntity<ClienteDTO> getByIdentificacion(@PathVariable long tipoDoc, @PathVariable String cedula) {
        Cliente cliente = clienteService.findByTiposDocAndCedula(new TiposDoc(tipoDoc), cedula);
        ClienteDTO clienteDTO = null;
        if(cliente != null) {
            clienteDTO = new ClienteDTO(cliente);
        }
        return ResponseEntity.ok(clienteDTO);
    }

    @PostMapping("/create")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ClienteDTO> save(@RequestBody ClienteDTO clienteDTO) {
        clienteService.save(new Cliente(clienteDTO));
        return ResponseEntity.ok(clienteDTO);
    }

    @PutMapping("/update")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ClienteDTO>  update(@RequestBody ClienteDTO clienteDTO) {
        clienteService.save(new Cliente(clienteDTO));
        return ResponseEntity.ok(clienteDTO);
    }

    @DeleteMapping("/deleteById/{codigo}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long codigo) throws ServiceException {
        clienteService.deleteById(codigo);
    }
}