package co.com.teayudamos.api.core.liquidacion.report;

import java.util.List;

public class SocioReport {

    private Long idSocio;
    private String nameSocio;
    private int porcentaje;
    private double valorPoIntereses;
    private double valorIntereses;
    private double valorPagado;
    private double valorEgreso;
    private double valorliquidacion;

    private List<SocioReport> socioDist;

    public Long getIdSocio() {
        return idSocio;
    }

    public void setIdSocio(Long idSocio) {
        this.idSocio = idSocio;
    }

    public String getNameSocio() {
        return nameSocio;
    }

    public void setNameSocio(String nameSocio) {
        this.nameSocio = nameSocio;
    }

    public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }

    public double getValorPoIntereses() {
        return valorPoIntereses;
    }

    public void setValorPoIntereses(double valorPoIntereses) {
        this.valorPoIntereses = valorPoIntereses;
    }

    public double getValorIntereses() {
        return valorIntereses;
    }

    public void setValorIntereses(double valorIntereses) {
        this.valorIntereses = valorIntereses;
    }

    public double getValorPagado() {
        return valorPagado;
    }

    public void setValorPagado(double valorPagado) {
        this.valorPagado = valorPagado;
    }

    public List<SocioReport> getSocioDist() {
        return socioDist;
    }

    public void setSocioDist(List<SocioReport> socioDist) {
        this.socioDist = socioDist;
    }

    public double getValorEgreso() {
        return valorEgreso;
    }

    public void setValorEgreso(double valorEgreso) {
        this.valorEgreso = valorEgreso;
    }

    public double getValorliquidacion() {
        return this.valorliquidacion;
    }

    public void setValorliquidacion(double valorliquidacion) {
        this.valorliquidacion = valorliquidacion;
    }
}
