package co.com.teayudamos.api.core.liquidacion.services;

import co.com.teayudamos.api.core.abonos.services.AbonoService;
import co.com.teayudamos.api.core.cierres.entity.Cierre;
import co.com.teayudamos.api.core.cierres.entity.CierreDetalle;
import co.com.teayudamos.api.core.cierres.services.CierreDetalleService;
import co.com.teayudamos.api.core.cierres.services.CierreService;
import co.com.teayudamos.api.core.liquidacion.report.Liquidacion;
import co.com.teayudamos.api.core.liquidacion.report.SocioReport;
import co.com.teayudamos.api.core.socios.entity.Socio;
import co.com.teayudamos.api.core.socios.entity.SocioDist;
import co.com.teayudamos.api.core.socios.services.SocioDistService;
import co.com.teayudamos.api.core.socios.services.SocioService;
import co.com.teayudamos.api.generic.controller.dto.EgresoDTO;
import co.com.teayudamos.api.generic.entity.Egreso;
import co.com.teayudamos.api.generic.services.GenericService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigInteger;
import java.security.acl.LastOwnerException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class LiquidacionServices {
    @Autowired
    private CierreService cierreService;
    @Autowired
    private SocioService socioService;
    @Autowired
    private AbonoService abonoService;
    @Autowired
    private SocioDistService socioDistService;
    @Autowired
    private CierreDetalleService cierreDetalleService;
    @Autowired
    private GenericService genericService;

    @Transactional
    public Liquidacion preLiquidacion() {

        Liquidacion liquidacion = new Liquidacion();

        Cierre cierre = cierreService.getCierreByFechaCierreIsNull();
        liquidacion.setPeriodo(cierre.getPeriodo());

        Object[][] balanceAbonos = abonoService.getBalanceByCierre(cierre.getCodigo());

        calcularLiquidacionSocio(liquidacion, balanceAbonos);

        Gson gson = new Gson();

        cierre.setDetalleCierre(gson.toJson(liquidacion));
        cierreService.update(cierre);

        return liquidacion;

    }

    @Transactional
    public Cierre liquidacion(List<SocioReport> sociosReport) {
        double sumLiquidado = 0;
        double sumEgresos = 0;

        Cierre cierre = cierreService.getCierreByFechaCierreIsNull();

        for (SocioReport socioReport : sociosReport) {
            sumLiquidado = sumLiquidado + socioReport.getValorIntereses();
            sumEgresos = sumEgresos + socioReport.getValorEgreso();

            CierreDetalle cierreDetalle = new CierreDetalle();

            cierreDetalle.setCierre(cierre);
            cierreDetalle.setSocio(new Socio(socioReport.getIdSocio()));
            cierreDetalle.setVlrLiquidado(socioReport.getValorIntereses());
            cierreDetalle.setVlrEgresos(socioReport.getValorEgreso());

            cierreDetalleService.save(cierreDetalle);
        }


        cierre.setFechaCierre(new Date());
        cierre.setVlrIntereses(sumLiquidado);
        cierre.setVlrEgresos(sumEgresos);
        cierreService.update(cierre);

        Cierre cierreNuevo = new Cierre();
        cierreNuevo.setPeriodo(getPeriodo(cierre.getPeriodo()));

        cierreService.save(cierreNuevo);

        return cierreNuevo;

    }

    private String getPeriodo(String periodo) {

        String month = periodo.substring((periodo.length() - 2), periodo.length());
        String year = periodo.substring(0, (periodo.length() - 2));

        if (month.contains("0")) {
            if (month.equals("09")) {
                month = "10";
            } else {
                int value = Integer.parseInt(month.substring(1));
                value = value + 1;
                month = "0" + value;
            }
        } else {
            if (month.equals("12")) {
                month = "01";
                int newYear = Integer.parseInt(year);
                newYear = newYear + 1;
                year = String.valueOf(newYear);
            } else {
                int newMonth = Integer.parseInt(month);
                newMonth = newMonth + 1;
                month = String.valueOf(newMonth);
            }
        }
        return year + month;
    }


    private void calcularLiquidacionSocio(Liquidacion liquidacion, Object[][] balanceAbonos) {
        List<Socio> socios = socioService.findAll();
        Map<Long, List<SocioDist>> mapDist = socioDistService.getMapDist();
        List<SocioReport> sociosReport = new ArrayList<>();
        Map<Long, SocioReport> mapSocioTotales = new HashMap<>();
        double totalInteresPeriodo = 0;
        for (Socio socio : socios) {
            SocioReport socioReport = new SocioReport();
            socioReport.setIdSocio(socio.getCodigo());
            socioReport.setNameSocio(socio.getNombre());
            socioReport.setPorcentaje(socio.getPorcentaje());

            loadAbanosPorSocio(balanceAbonos, socio.getCodigo(), socioReport);

            totalInteresPeriodo = totalInteresPeriodo + socioReport.getValorIntereses();

            List<SocioReport> distSocios = new ArrayList<>();

            double valor = (socioReport.getValorIntereses() * (new Double(socio.getPorcentaje())/ 100));

            socioReport.setValorPoIntereses(valor);

            sumTotalBySocio(mapSocioTotales, socioReport, valor);

//            distSocios.add(socioReport);


            List<SocioDist> listDist = mapDist.get(socio.getCodigo());
            if (listDist != null && !listDist.isEmpty()) {
                for (SocioDist socioDist : listDist) {
                    SocioReport socioReportDist = new SocioReport();
                    socioReportDist.setIdSocio(socioDist.getSocioDist().getCodigo());
                    socioReportDist.setNameSocio(socioDist.getSocioDist().getNombre());
                    socioReportDist.setPorcentaje(socioDist.getPorcentaje());
                    double valorDist = socioReport.getValorIntereses() * (new Double(socioDist.getPorcentaje()) / 100);
                    socioReportDist.setValorPoIntereses(valorDist);
                    distSocios.add(socioReportDist);
                    sumTotalBySocio(mapSocioTotales, socioReportDist, valorDist);
                }

            }
            socioReport.setSocioDist(distSocios);
            sociosReport.add(socioReport);
        }
        liquidacion.setSocios(sociosReport);
        liquidacion.setSociosTotales(new ArrayList<>(mapSocioTotales.values()));
        liquidacion.setTotalInteresPeriodo(totalInteresPeriodo);
        calculateEgresos(liquidacion);

    }

    private void calculateEgresos(Liquidacion liquidacion) {


        List<Egreso> egresos = genericService.getAllEgresos();
        List<EgresoDTO> egresoDTOS = genericService.getAllEgresosDTO(egresos);
        liquidacion.setEgresos(egresoDTOS);
        for (SocioReport socioReport : liquidacion.getSociosTotales()) {
            double totalEgresos = 0;
            for (EgresoDTO egresoDTO : egresoDTOS) {
                totalEgresos = totalEgresos + (socioReport.getValorIntereses() * (new Double(egresoDTO.getPorcentaje()) / 100));
            }
            socioReport.setValorEgreso(totalEgresos);
            socioReport.setValorliquidacion(socioReport.getValorIntereses() - socioReport.getValorEgreso());
        }
        liquidacion.setEgresos(egresoDTOS);


    }

    private void sumTotalBySocio(Map<Long, SocioReport> mapSocioTotales, SocioReport socioReport, double valor) {
        SocioReport socioTotalReport = mapSocioTotales.get(socioReport.getIdSocio());
        if (socioTotalReport == null) {
            socioTotalReport = new SocioReport();
            socioTotalReport.setIdSocio(socioReport.getIdSocio());
            socioTotalReport.setNameSocio(socioReport.getNameSocio());
        }
        double valorInteres = socioTotalReport.getValorIntereses() + valor;
        socioTotalReport.setValorIntereses(valorInteres);
        mapSocioTotales.put(socioReport.getIdSocio(), socioTotalReport);

    }


    private void loadAbanosPorSocio(Object[][] balanceAbonos, Long idSocio, SocioReport socioReport) {
        for (Object[] data : balanceAbonos) {
            if (idSocio == ((BigInteger) data[0]).longValue()) {
                socioReport.setValorIntereses((Double) data[1]);
                socioReport.setValorPagado((Double) data[1]);
                break;
            }
        }
    }


}
