package co.com.teayudamos.api.core.cuentasbancarias.services;

import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.core.cuentasbancarias.controller.dto.CuentaBancariaDTO;
import co.com.teayudamos.api.core.cuentasbancarias.entity.CuentasBancaria;
import co.com.teayudamos.api.exception.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class CuentaBancariaService {

    @Autowired
    private ICuentaBancariaRepository cuentaBancariaRepository;

    public void  save(CuentasBancaria cuentasBancarias) throws ServiceException {

        Boolean existCuenta = cuentaBancariaRepository.existsByDsCuentaAndCliente(cuentasBancarias.getDsCuenta(), cuentasBancarias.getCliente());

        if(!existCuenta) {

            cuentasBancarias.setFechaCreaion(new Date());
            cuentasBancarias.setFechaModificacion(new Date());
            cuentasBancarias.setSnActivo(true);
            cuentasBancarias.setSnCuentaPrincipal("S");

            cuentaBancariaRepository.save(cuentasBancarias);
        }else {
            throw new ServiceException("El número de cuenta bancaria ya existe, si la desea actualizar, por favor eliminarla y crear una nueva", 1);
        }
    }


    public void deleteById(Long codigo){
        cuentaBancariaRepository.deleteById(codigo);
    }

     public List<CuentasBancaria> findAllByCliente(Cliente cliente){
        return cuentaBancariaRepository.findAllByCliente(cliente);
    }

    public List<CuentaBancariaDTO> getAllCuentasBancariaDTO(List<CuentasBancaria> listCuentas){
        List<CuentaBancariaDTO> cuentasBancarias = new ArrayList<>();
        for (CuentasBancaria cuentasBancaria : listCuentas) {
            CuentaBancariaDTO cuentaBancariaDTO = new CuentaBancariaDTO(cuentasBancaria);
            cuentasBancarias.add(cuentaBancariaDTO);
        }
        return cuentasBancarias;

    }
}
