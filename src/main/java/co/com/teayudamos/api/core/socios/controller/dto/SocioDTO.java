package co.com.teayudamos.api.core.socios.controller.dto;

import co.com.teayudamos.api.core.socios.entity.Socio;

import java.io.Serializable;

public class SocioDTO implements Serializable {

    private long codigo;
    private long tipoDoc;
    private String cedula;
    private String nombre;
    private boolean snActivo;

    public SocioDTO(Socio socio) {
        this.codigo = socio.getCodigo();
        this.tipoDoc = socio.getTiposDoc().getCodigo();
        this.cedula = socio.getCedula();
        this.nombre = socio.getNombre();
        this.snActivo = socio.isSnActivo();
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public long getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(long tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isSnActivo() {
        return snActivo;
    }

    public void setSnActivo(boolean snActivo) {
        this.snActivo = snActivo;
    }
}
