package co.com.teayudamos.api.core.cierres.controller.dto;

import co.com.teayudamos.api.core.cierres.entity.Cierre;

import java.util.Date;

public class CierreDTO {

    private Long codigo;

    private String periodo;

    private Date fechaCierre;

    private double vlrIntereses;

    private double vlrEgresos;

    private String detalleCierre;

    public CierreDTO(Cierre cierre) {
        this.codigo = cierre.getCodigo();
        this.periodo = cierre.getPeriodo();
        this.fechaCierre = cierre.getFechaCierre();
        this.vlrIntereses = cierre.getVlrIntereses();
        this.vlrEgresos = cierre.getVlrEgresos();
        this.detalleCierre = cierre.getDetalleCierre();
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public Date getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public double getVlrIntereses() {
        return vlrIntereses;
    }

    public void setVlrIntereses(double vlrIntereses) {
        this.vlrIntereses = vlrIntereses;
    }

    public double getVlrEgresos() {
        return vlrEgresos;
    }

    public void setVlrEgresos(double vlrEgresos) {
        this.vlrEgresos = vlrEgresos;
    }

    public String getDetalleCierre() {
        return detalleCierre;
    }

    public void setDetalleCierre(String detalleCierre) {
        this.detalleCierre = detalleCierre;
    }
}
