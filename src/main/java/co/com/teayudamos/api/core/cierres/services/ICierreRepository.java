package co.com.teayudamos.api.core.cierres.services;

import co.com.teayudamos.api.core.cierres.entity.Cierre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ICierreRepository extends JpaRepository<Cierre, Long> {

    Cierre getCierreByFechaCierreIsNull();

}
