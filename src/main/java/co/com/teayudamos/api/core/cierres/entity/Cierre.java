package co.com.teayudamos.api.core.cierres.entity;

import co.com.teayudamos.api.core.socios.entity.Socio;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "TAYU_CIERRES")
public class Cierre {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long codigo;

    @Column(name = "DSPERIODO", nullable = false, length = 6)
    private String periodo;

    @Column(name = "FECIERRE", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date fechaCierre;

    @Column(name = "NMVLR_INERESES", nullable = true, length = 15, precision = 2)
    private double vlrIntereses;

    @Column(name = "NMVLR_EGRESOS", nullable = true, length = 15, precision = 2)
    private double vlrEgresos;

    @Column(name = "DSDETALLEJSON", nullable = true, length = 2000)
    private String detalleCierre;

    public Cierre() {
    }

    public Cierre(Long codigo) {
        this.codigo = codigo;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public Date getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public double getVlrIntereses() {
        return vlrIntereses;
    }

    public void setVlrIntereses(double vlrIntereses) {
        this.vlrIntereses = vlrIntereses;
    }

    public double getVlrEgresos() {
        return vlrEgresos;
    }

    public void setVlrEgresos(double vlrEgresos) {
        this.vlrEgresos = vlrEgresos;
    }

    public String getDetalleCierre() {
        return detalleCierre;
    }

    public void setDetalleCierre(String detalleCierre) {
        this.detalleCierre = detalleCierre;
    }
}
