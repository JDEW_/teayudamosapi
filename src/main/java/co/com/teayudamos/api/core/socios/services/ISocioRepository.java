package co.com.teayudamos.api.core.socios.services;

import co.com.teayudamos.api.core.socios.entity.Socio;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ISocioRepository extends JpaRepository<Socio, Long> {

}
