package co.com.teayudamos.api.core.cierres.services;

import co.com.teayudamos.api.core.cierres.entity.Cierre;
import co.com.teayudamos.api.core.cierres.entity.CierreDetalle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CierreDetalleService {
    @Autowired
    private ICierreDetalleRepository cierreDetalleRepository;

    public void save(CierreDetalle cierreDetalle){
        cierreDetalleRepository.save(cierreDetalle);
    }

    public void deleteAllByCierre(Cierre cierre){
        cierreDetalleRepository.deleteAllByCierre(cierre);
    }
}
