package co.com.teayudamos.api.core.reportes.report;

public class Balance implements Comparable<Balance>{

    private double totalPrestado;
    private double totalPagado;
    private double totalInteres;
    private String periodo;
    private String namePeriodo;

    public Balance() {
    }

    public double getTotalPrestado() {
        return totalPrestado;
    }

    public void setTotalPrestado(double totalPrestado) {
        this.totalPrestado = totalPrestado;
    }

    public double getTotalPagado() {
        return totalPagado;
    }

    public void setTotalPagado(double totalPagado) {
        this.totalPagado = totalPagado;
    }

    public double getTotalInteres() {
        return totalInteres;
    }

    public void setTotalInteres(double totalInteres) {
        this.totalInteres = totalInteres;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getNamePeriodo() {
        return namePeriodo;
    }

    public void setNamePeriodo(String namePeriodo) {
        this.namePeriodo = namePeriodo;
    }

    @Override
    public int compareTo(Balance o) {
        return this.periodo.compareTo(o.periodo);
    }
}
