package co.com.teayudamos.api.core.liquidacion.controller;

import co.com.teayudamos.api.core.cierres.controller.dto.CierreDTO;
import co.com.teayudamos.api.core.cierres.entity.Cierre;
import co.com.teayudamos.api.core.liquidacion.report.Liquidacion;
import co.com.teayudamos.api.core.liquidacion.report.SocioReport;
import co.com.teayudamos.api.core.liquidacion.services.LiquidacionServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200", "https://teayudamos-99.firebaseapp.com", "https://teayudamos-all.firebaseapp.com"})
@RestController
@RequestMapping("/liquidacion/v1/")
public class LiquidacionController {

    @Autowired
    private LiquidacionServices liquidacionServices;

    @GetMapping("/findPreliquidar")
    public ResponseEntity<Liquidacion> findPreliquidar() {

        Liquidacion liquidacion = liquidacionServices.preLiquidacion();

        return ResponseEntity.ok(liquidacion);
    }


    @PostMapping("/liquidar")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CierreDTO> save(@RequestBody List<SocioReport> sociosReport) {

        Cierre cierre = liquidacionServices.liquidacion(sociosReport);

        return ResponseEntity.ok(new CierreDTO(cierre));
    }
}
