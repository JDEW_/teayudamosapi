package co.com.teayudamos.api.core.prestamos.services;

import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.core.prestamos.entity.Prestamo;
import co.com.teayudamos.api.core.prestamos.type.Estado;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface IPrestamoRepository extends JpaRepository<Prestamo, Long> {

    List<Prestamo> findAllByEstadoIn(List<Estado> estados);

    Long countByCliente(Cliente cliente);

    @Query(value = "SELECT DATE_FORMAT(TP.FEENTREGA, '%Y%m'), SUM(TP.NMVALOR) AS totalPrestado FROM tayu_prestamos TP WHERE TP.CDESTADO <> 2 AND DATE_FORMAT(TP.FEENTREGA, '%Y') = ?1  GROUP BY DATE_FORMAT(TP.FEENTREGA, '%Y%m') ORDER BY DATE_FORMAT(TP.FEENTREGA, '%Y%m')", nativeQuery = true)
    Object[][] getBalanceByYear(String year);

    @Query(value = "SELECT cliente.id as idCliente, prestamo.nmvalor_cuota as valorCuota, cliente.dstelefono as telefono, cliente.dsnombre_ref as nombreReferencia, cliente.dstel_rer as telefonoReferencia, prestamo.id as idPrestamo, prestamo.nmvalor as valorPrestamo,( prestamo.nmvalor - (SELECT IFNULL(SUM(abono.nmvlr_cuota_pagado), 0) FROM tayu_abonos abono WHERE abono.id_prestamo = prestamo.id and snactivo = 1) ) as vlrRestante FROM tayu_prestamos prestamo LEFT JOIN tayu_clientes cliente ON prestamo.id_cliente = cliente.id WHERE TP.CDESTADO <> 2 AND ( SELECT IFNULL (SUM(nmvlr_cuota), 0) FROM tayu_abonos where snactivo = 1 and id_prestamo = prestamo.id ) < ( ( TIMESTAMPDIFF(MONTH, prestamo.fepago, CURDATE()) + 1 ) * prestamo.nmvalor_cuota )", nativeQuery = true)
    Object[][] getPrestamosPendientesPago();
}