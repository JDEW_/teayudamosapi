package co.com.teayudamos.api.core.cierres.entity;

import co.com.teayudamos.api.core.socios.entity.Socio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "TAYU_CIERRES_DET")
public class CierreDetalle {

    public CierreDetalle() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long codigo;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name="ID_CIERRE", nullable=false)
    private Cierre cierre;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_SOCIO", nullable = false)
    private Socio socio;

    @Column(name = "NMVLR_LIQUIDADO", nullable = false, length = 15, precision = 2)
    private double vlrLiquidado;

    @Column(name = "NMVLR_EGRESO", nullable = false, length = 15, precision = 2)
    private double vlrEgresos;

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public Cierre getCierre() {
        return cierre;
    }

    public void setCierre(Cierre cierre) {
        this.cierre = cierre;
    }

    public Socio getSocio() {
        return socio;
    }

    public void setSocio(Socio socio) {
        this.socio = socio;
    }

    public double getVlrLiquidado() {
        return vlrLiquidado;
    }

    public void setVlrLiquidado(double vlrLiquidado) {
        this.vlrLiquidado = vlrLiquidado;
    }

    public double getVlrEgresos() {
        return vlrEgresos;
    }

    public void setVlrEgresos(double vlrEgresos) {
        this.vlrEgresos = vlrEgresos;
    }
}
