package co.com.teayudamos.api.core.clientes.services;

import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.generic.entity.TiposDoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IClienteRepository extends JpaRepository<Cliente, Long> {

    Cliente findByTiposDocAndCedula(TiposDoc tiposDoc, String cedula);

    Long countByCodeudor(Cliente referido);

}
