package co.com.teayudamos.api.core.socios.entity;

import co.com.teayudamos.api.generic.entity.TiposDoc;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "TAYU_SOCIOS")
public class Socio implements Serializable {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public Socio(){}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long codigo;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_CDTIPO_DOC", nullable = false)
    private TiposDoc tiposDoc;

    @Column(name = "DSCEDULA", nullable = false, length = 20)
    private String cedula;

    @Column(name = "DSNOMBRE", nullable = false, length = 200)
    private String nombre;

    @Column(name = "SNACTIVO", nullable = false, length = 1)
    private boolean snActivo;

    @Column(name = "ID_USUARIO_CREACION", nullable = false, length = 15)
    private Long usuarioCreacion;

    @Column(name = "FECREACION", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date fechaCreaion;

    @Column(name = "FEMODIFICACION", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date fechaModificacion;

    @Column(name = "NMPORCENTAJE", nullable = false, length = 3)
    private int porcentaje;

    public Socio(long codigo){
        this.codigo = codigo;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public TiposDoc getTiposDoc() {
        return tiposDoc;
    }

    public void setTiposDoc(TiposDoc tiposDoc) {
        this.tiposDoc = tiposDoc;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isSnActivo() {
        return snActivo;
    }

    public void setSnActivo(boolean snActivo) {
        this.snActivo = snActivo;
    }

    public Long getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(Long usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Date getFechaCreaion() {
        return fechaCreaion;
    }

    public void setFechaCreaion(Date fechaCreaion) {
        this.fechaCreaion = fechaCreaion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }
}
