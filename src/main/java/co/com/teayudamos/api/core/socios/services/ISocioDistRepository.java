package co.com.teayudamos.api.core.socios.services;

import co.com.teayudamos.api.core.socios.entity.Socio;
import co.com.teayudamos.api.core.socios.entity.SocioDist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ISocioDistRepository extends JpaRepository<SocioDist, Long> {

    List<SocioDist> findAllBySocio(Socio socio);

}
