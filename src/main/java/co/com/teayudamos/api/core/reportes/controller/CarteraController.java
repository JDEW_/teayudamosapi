package co.com.teayudamos.api.core.reportes.controller;

import co.com.teayudamos.api.core.reportes.services.CarteraServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import co.com.teayudamos.api.core.reportes.report.Cartera;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200", "https://teayudamos-99.firebaseapp.com", "https://teayudamos-all.firebaseapp.com"})
@RestController
@RequestMapping("/cartera/v1/")
public class CarteraController {

    @Autowired
    private CarteraServices carteraServices;

    @GetMapping("/findCartera/")
    public ResponseEntity<List<Cartera>> findCartera() {
        List<Cartera> carteraReport = carteraServices.generateReportCartera();
        return ResponseEntity.ok(carteraReport);
    }
}