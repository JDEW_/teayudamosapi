package co.com.teayudamos.api.core.socios.controller;

import co.com.teayudamos.api.core.clientes.controller.dto.ClienteDTO;
import co.com.teayudamos.api.core.clientes.entity.Cliente;
import co.com.teayudamos.api.core.clientes.services.ClienteService;
import co.com.teayudamos.api.core.socios.controller.dto.SocioDTO;
import co.com.teayudamos.api.core.socios.entity.Socio;
import co.com.teayudamos.api.core.socios.services.SocioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200", "https://teayudamos-99.firebaseapp.com", "https://teayudamos-all.firebaseapp.com"})
@RestController
@RequestMapping("/socios/v1/")
public class SocioController {

    @Autowired
    private SocioService socioService;

    @GetMapping("/findAll")
    public ResponseEntity<List<SocioDTO>> findAll() {
        List<Socio> socios = socioService.findAll();
        return ResponseEntity.ok(socioService.getAllSociosDTO(socios));
    }
}
