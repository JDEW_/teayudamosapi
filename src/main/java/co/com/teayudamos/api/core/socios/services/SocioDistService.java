package co.com.teayudamos.api.core.socios.services;

import co.com.teayudamos.api.core.socios.entity.Socio;
import co.com.teayudamos.api.core.socios.entity.SocioDist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SocioDistService {

    @Autowired
    private ISocioDistRepository socioDistRepository;

    public List<SocioDist> findAllBySocio(Socio socio){
        return socioDistRepository.findAllBySocio(socio);
    }

    public Map<Long, List<SocioDist>> getMapDist(){
        Map<Long, List<SocioDist>> map = new HashMap<>();
        List<SocioDist> list = socioDistRepository.findAll();
        for (SocioDist socioDist: list) {
            List<SocioDist> socioDists = map.get(socioDist.getSocio().getCodigo());
            if(socioDists == null){
                socioDists = new ArrayList<>();
                socioDists.add(socioDist);
                map.put(socioDist.getSocio().getCodigo(), socioDists);
            }else {
                socioDists.add(socioDist);
                map.put(socioDist.getSocio().getCodigo(), socioDists);
            }
        }

        return map;
    }


}
