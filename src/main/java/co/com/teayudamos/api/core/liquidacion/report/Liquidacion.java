package co.com.teayudamos.api.core.liquidacion.report;

import co.com.teayudamos.api.generic.controller.dto.EgresoDTO;

import java.util.List;

public class Liquidacion {
    private String periodo;
    private List<SocioReport> socios;
    private List<SocioReport> sociosTotales;
    private List<EgresoDTO> egresos;
    private double totalInteresPeriodo;

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public List<SocioReport> getSocios() {
        return socios;
    }

    public void setSocios(List<SocioReport> socios) {
        this.socios = socios;
    }

    public List<SocioReport> getSociosTotales() {
        return sociosTotales;
    }

    public void setSociosTotales(List<SocioReport> sociosTotales) {
        this.sociosTotales = sociosTotales;
    }

    public double getTotalInteresPeriodo() {
        return totalInteresPeriodo;
    }

    public void setTotalInteresPeriodo(double totalInteresPeriodo) {
        this.totalInteresPeriodo = totalInteresPeriodo;
    }

    public List<EgresoDTO> getEgresos() {
        return egresos;
    }

    public void setEgresos(List<EgresoDTO> egresos) {
        this.egresos = egresos;
    }
}
