package co.com.teayudamos.api.core.reportes.services;

import co.com.teayudamos.api.core.abonos.services.AbonoService;
import co.com.teayudamos.api.core.prestamos.services.PrestamoService;
import co.com.teayudamos.api.core.reportes.report.Balance;
import co.com.teayudamos.api.generic.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class BalanceServices {

    @Autowired
    private PrestamoService prestamoService;
    @Autowired
    private AbonoService abonoService;

    public List<Balance> generateReportBalance(String year) {

        Balance balanceTotales = new Balance();
        balanceTotales.setNamePeriodo("TOTALES DEL AÑO " + year);
        balanceTotales.setPeriodo(year + "13");

        HashMap<String, Balance> mapReport = loadInfoMap(year);
        Object[][] balancePrestamo = prestamoService.getBalanceByYear(year);
        for (Object[] data : balancePrestamo) {
            Balance balance = mapReport.get(data[0]);
            balance.setTotalPrestado((Double) data[1]);
            balanceTotales.setTotalPrestado(balanceTotales.getTotalPrestado() + (Double) data[1]);

        }

        Object[][] balanceAbonos = abonoService.getBalanceByYear(year);
        for (Object[] data : balanceAbonos) {
            Balance balance = mapReport.get(data[0]);
            balance.setTotalPagado((Double) data[1]);
            balance.setTotalInteres((Double) data[2]);
            balanceTotales.setTotalPagado(balanceTotales.getTotalPagado() + (Double) data[1]);
            balanceTotales.setTotalInteres(balanceTotales.getTotalInteres() + (Double) data[2]);
        }

        List<Balance> list = new ArrayList<>(mapReport.values());
        list.add(balanceTotales);

        Collections.sort(list);

        return list;
    }

    private HashMap<String, Balance> loadInfoMap(String year) {

        HashMap<String, Balance> mapBalance = new HashMap<>();

        Map<String, String> mapMonths = Utils.getMnths();

        for (String idMonth : mapMonths.keySet()) {
            Balance balance = new Balance();
            balance.setPeriodo(year + idMonth);
            balance.setNamePeriodo(mapMonths.get(idMonth));
            mapBalance.put(balance.getPeriodo(), balance);
        }

        return mapBalance;
    }
}
