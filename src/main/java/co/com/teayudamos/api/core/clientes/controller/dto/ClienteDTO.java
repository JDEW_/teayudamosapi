package co.com.teayudamos.api.core.clientes.controller.dto;

import co.com.teayudamos.api.core.clientes.entity.Cliente;

import java.io.Serializable;

import java.util.List;

public class ClienteDTO implements Serializable {

    private long codigo;
    private long tipoDoc;
    private String cedula;
    private String nombre;
    private String direccion;
    private String telefono;
    private String celular;
    private String email;
    private boolean snActivo;
    private Long referidoId;
    private Long socioId;
    private String socioNombre;
    private String telReferencia;
    private String nombreReferencia;


    public ClienteDTO() {
    }

    public
    ClienteDTO(Cliente cliente){
        if(cliente != null) {
            this.codigo = cliente.getCodigo();
            this.tipoDoc = cliente.getTiposDoc().getCodigo();
            this.cedula = cliente.getCedula();
            this.nombre = cliente.getNombre();
            this.direccion = cliente.getDireccion();
            this.telefono = cliente.getTelefono();
            this.celular = cliente.getCelular();
            this.email = cliente.getEmail();
            this.snActivo = cliente.isSnActivo();
            this.referidoId = (cliente.getCodeudor() != null ? cliente.getCodeudor().getCodigo() : null);
            this.socioId = cliente.getSocio().getCodigo();
            this.socioNombre = cliente.getSocio().getNombre();
            this.nombreReferencia = cliente.getNombreReferencia();
            this.telReferencia = cliente.getTelReferencia();
        }

    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public long getTipoDoc() {
        return tipoDoc;
    }

    public void setTipoDoc(long tipoDoc) {
        this.tipoDoc = tipoDoc;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isSnActivo() {
        return snActivo;
    }

    public void setSnActivo(boolean snActivo) {
        this.snActivo = snActivo;
    }

    public Long getReferidoId() {
        return referidoId;
    }

    public void setReferidoId(Long referidoId) {
        this.referidoId = referidoId;
    }

    public Long getSocioId() {
        return socioId;
    }

    public void setSocioId(Long socioId) {
        this.socioId = socioId;
    }

    public String getSocioNombre() {
        return socioNombre;
    }

    public void setSocioNombre(String socioNombre) {
        this.socioNombre = socioNombre;
    }

    public String getTelReferencia() {
        return telReferencia;
    }

    public void setTelReferencia(String telReferencia) {
        this.telReferencia = telReferencia;
    }

    public String getNombreReferencia() {
        return nombreReferencia;
    }

    public void setNombreReferencia(String nombreReferencia) {
        this.nombreReferencia = nombreReferencia;
    }
}