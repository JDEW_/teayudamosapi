package co.com.teayudamos.api.core.abonos.controller.dto;

import co.com.teayudamos.api.core.abonos.entity.Abono;

import java.io.Serializable;
import java.util.Date;

public class AbonoDTO implements Serializable {

    private Long codigo;
    private double valor;
    private double interes;
    private int cuota;
    private double interesPagado;
    private double valorPagado;
    private Date fechaAbono;
    private Date fechaCreacion;
    private Long codigoPrestamo;
    private Long codigoMedioPago;
    private Long codigoCierre;
    private Long usuarioCreacion;
    private Long usuarioActualizacion;
    private double totalPrestamo;
    private String nombreCliente;
    private String estado;
    private double totalRestante;
    private double totalAbonado;
    private double vlrCuotaSugerida;
    private boolean snActivo;

    public AbonoDTO() {
    }

    public AbonoDTO(Abono abono) {
        this.codigo = abono.getCodigo();
        this.valor = abono.getValor();
        this.interes = abono.getInteres();
        this.cuota = abono.getCuota();
        this.interesPagado = abono.getInteresPagado();
        this.valorPagado = abono.getValorPagado();
        this.fechaAbono = abono.getFechaAbono();
        this.fechaCreacion = abono.getFechaCreacion();
        this.codigoPrestamo = abono.getPrestamo().getCodigo();
        this.codigoMedioPago = abono.getMedioPago().getCodigo();
        this.codigoCierre = abono.getCierre().getCodigo();
        this.usuarioCreacion = abono.getUsuarioCreacion();
        this.usuarioActualizacion = abono.getUsuarioActualizacion();
        this.snActivo = abono.isSnActivo();
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getInteres() {
        return interes;
    }

    public void setInteres(double interes) {
        this.interes = interes;
    }

    public int getCuota() {
        return cuota;
    }

    public void setCuota(int cuota) {
        this.cuota = cuota;
    }

    public double getInteresPagado() {
        return interesPagado;
    }

    public void setInteresPagado(double interesPagado) {
        this.interesPagado = interesPagado;
    }

    public double getValorPagado() {
        return valorPagado;
    }

    public void setValorPagado(double valorPagado) {
        this.valorPagado = valorPagado;
    }

    public Date getFechaAbono() {
        return fechaAbono;
    }

    public void setFechaAbono(Date fechaAbono) {
        this.fechaAbono = fechaAbono;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Long getCodigoPrestamo() {
        return codigoPrestamo;
    }

    public void setCodigoPrestamo(Long codigoPrestamo) {
        this.codigoPrestamo = codigoPrestamo;
    }

    public Long getCodigoMedioPago() {
        return codigoMedioPago;
    }

    public void setCodigoMedioPago(Long codigoMedioPago) {
        this.codigoMedioPago = codigoMedioPago;
    }

    public Long getCodigoCierre() {
        return codigoCierre;
    }

    public void setCodigoCierre(Long codigoCierre) {
        this.codigoCierre = codigoCierre;
    }

    public Long getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(Long usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Long getUsuarioActualizacion() {
        return usuarioActualizacion;
    }

    public void setUsuarioActualizacion(Long usuarioActualizacion) {
        this.usuarioActualizacion = usuarioActualizacion;
    }

    public double getTotalPrestamo() {
        return totalPrestamo;
    }

    public void setTotalPrestamo(double totalPrestamo) {
        this.totalPrestamo = totalPrestamo;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public double getTotalRestante() {
        return totalRestante;
    }

    public void setTotalRestante(double totalRestante) {
        this.totalRestante = totalRestante;
    }

    public double getTotalAbonado() {
        return totalAbonado;
    }

    public void setTotalAbonado(double totalAbonado) {
        this.totalAbonado = totalAbonado;
    }

    public double getVlrCuotaSugerida() {
        return vlrCuotaSugerida;
    }

    public void setVlrCuotaSugerida(double vlrCuotaSugerida) {
        this.vlrCuotaSugerida = vlrCuotaSugerida;
    }

    public boolean isSnActivo() {
        return snActivo;
    }

    public void setSnActivo(boolean snActivo) {
        this.snActivo = snActivo;
    }
}