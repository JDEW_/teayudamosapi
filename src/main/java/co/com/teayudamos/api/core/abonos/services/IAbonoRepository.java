package co.com.teayudamos.api.core.abonos.services;

import co.com.teayudamos.api.core.abonos.entity.Abono;
import co.com.teayudamos.api.core.prestamos.entity.Prestamo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IAbonoRepository extends JpaRepository<Abono, Long> {

    List<Abono> findByPrestamoAndSnActivo(Prestamo prestamo, boolean snActivo);

    @Query("SELECT SUM(a.valorPagado) from Abono a WHERE a.snActivo = '1' AND a.prestamo = ?1")
    Double sumAbonosByPrestamo(Prestamo prestamo);


    @Query("SELECT A FROM Abono A WHERE A.cuota = (SELECT MAX(R.cuota) FROM Abono R WHERE R.snActivo = '1' AND R.prestamo = ?1 ) AND A.snActivo = '1' AND A.prestamo = ?1")
    List<Abono> getLastAbono(Prestamo prestamo);

    Long countByPrestamoAndSnActivo(Prestamo prestamo, boolean snActivo);

    @Query(value = "SELECT DATE_FORMAT(TA.FEABONO, '%Y%m'), SUM(TA.NMVLR_CUOTA_PAGADO), SUM(TA.NMVLR_INTERES_PAGADO) FROM tayu_abonos TA WHERE TA.SNACTIVO = 1 AND DATE_FORMAT(TA.FEABONO, '%Y') = ?1 GROUP BY DATE_FORMAT(TA.FEABONO, '%Y%m') ORDER BY DATE_FORMAT(TA.FEABONO, '%Y%m')", nativeQuery = true)
    Object[][] getBalanceByYear(String year);

    @Query(value = "SELECT TC.ID_SOCIO, SUM(TA.NMVLR_CUOTA_PAGADO), SUM(TA.NMVLR_INTERES_PAGADO) FROM tayu_abonos TA, tayu_prestamos TP, tayu_clientes TC WHERE TA.ID_PRESTAMO = TP.ID AND TP.ID_CLIENTE = TC.ID AND TA.SNACTIVO = 1 AND TA.ID_CIERRE = ?1 GROUp BY TC.ID_SOCIO", nativeQuery = true)
    Object[][] getBalanceByCierre(Long idCierre);
}