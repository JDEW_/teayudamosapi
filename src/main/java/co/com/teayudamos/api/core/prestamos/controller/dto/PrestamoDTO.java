package co.com.teayudamos.api.core.prestamos.controller.dto;

import co.com.teayudamos.api.core.prestamos.entity.Prestamo;

import java.io.Serializable;
import java.util.Date;

public class PrestamoDTO implements Serializable {

    private Long codigo;
    private Double valor;
    private Date feCreacion;
    private Integer cuotas;
    private Date feActualizacion;
    private Date feEntrega;
    private Integer estado;
    private String nombreEstado;
    private String usuarioCreacion;
    private Long idCliente;
    private Double valorCuotaInteres;
    private Double valorCuota;
    private Date fePago;
    private Date feMaxPago;
    private String nombreCliente;
    private Double totalRestante;
    private Double totalAbonado;

    public PrestamoDTO() {
    }

    public PrestamoDTO(Long codigo) {
        this.codigo = codigo;
    }

    public PrestamoDTO(Prestamo prestamo) {
        this.codigo = prestamo.getCodigo();
        this.valor = prestamo.getValor();
        this.feCreacion = prestamo.getFechaCreacion();
        this.cuotas = prestamo.getCuotas();
        this.feActualizacion = prestamo.getFechaActualizacion();
        this.feEntrega = prestamo.getFechaEntrega();
        this.fePago = prestamo.getFePago();
        this.feMaxPago = prestamo.getFeMaxPago();
        this.valorCuotaInteres = prestamo.getValorCuotaInteres();
        this.valorCuota = prestamo.getValorCuota();
        this.estado = prestamo.getEstado().getCodigo();
        this.nombreEstado = prestamo.getEstado().getNombre();
        this.idCliente = prestamo.getCliente().getCodigo();
        this.nombreCliente = prestamo.getCliente().getNombre();

    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Date getFeCreacion() {
        return feCreacion;
    }

    public void setFeCreacion(Date feCreacion) {
        this.feCreacion = feCreacion;
    }

    public Integer getCuotas() {
        return cuotas;
    }

    public void setCuotas(Integer cuotas) {
        this.cuotas = cuotas;
    }

    public Date getFeActualizacion() {
        return feActualizacion;
    }

    public void setFeActualizacion(Date feActualizacion) {
        this.feActualizacion = feActualizacion;
    }

    public Date getFeEntrega() {
        return feEntrega;
    }

    public void setFeEntrega(Date feEntrega) {
        this.feEntrega = feEntrega;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }

    public String getUsuarioCreacion() {
        return usuarioCreacion;
    }

    public void setUsuarioCreacion(String usuarioCreacion) {
        this.usuarioCreacion = usuarioCreacion;
    }

    public Long getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(Long idCliente) {
        this.idCliente = idCliente;
    }

    public Double getValorCuotaInteres() {
        return valorCuotaInteres;
    }

    public void setValorCuotaInteres(Double valorCuotaInteres) {
        this.valorCuotaInteres = valorCuotaInteres;
    }

    public Double getValorCuota() {
        return valorCuota;
    }

    public void setValorCuota(Double valorCuota) {
        this.valorCuota = valorCuota;
    }

    public Date getFePago() {
        return fePago;
    }

    public void setFePago(Date fePago) {
        this.fePago = fePago;
    }

    public Date getFeMaxPago() {
        return feMaxPago;
    }

    public void setFeMaxPago(Date feMaxPago) {
        this.feMaxPago = feMaxPago;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public Double getTotalRestante() {
        return totalRestante;
    }

    public void setTotalRestante(Double totalRestante) {
        this.totalRestante = totalRestante;
    }

    public Double getTotalAbonado() {
        return totalAbonado;
    }

    public void setTotalAbonado(Double totalAbonado) {
        this.totalAbonado = totalAbonado;
    }
}