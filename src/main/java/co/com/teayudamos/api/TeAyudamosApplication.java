package co.com.teayudamos.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class TeAyudamosApplication {

	public static void main(String[] args) {
		SpringApplication.run(TeAyudamosApplication.class, args);
	}
}
