package co.com.teayudamos.api.generic.services;

import co.com.teayudamos.api.generic.controller.dto.EgresoDTO;
import co.com.teayudamos.api.generic.controller.dto.GenericDTO;
import co.com.teayudamos.api.generic.entity.Banco;
import co.com.teayudamos.api.generic.entity.Egreso;
import co.com.teayudamos.api.generic.entity.MedioPago;
import co.com.teayudamos.api.generic.entity.Parametro;
import co.com.teayudamos.api.generic.entity.TiposDoc;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GenericService {

    @Autowired
    private IBancoRepository bancoRepository;
    @Autowired
    private IEgresoRepository egresoRepository;
    @Autowired
    private IMedioPagoRepository medioPagoRepository;
    @Autowired
    private IParametroRepository parametroRepository;
    @Autowired
    private ITiposDocRepository tiposDocRepository;

    public List<Banco> getAllBancos(){
        return bancoRepository.findAll();
    }

    public List<Egreso> getAllEgresos(){
        return egresoRepository.findAll();
    }

    public List<MedioPago> getAllMedioPagos(){
        return medioPagoRepository.findAll();
    }

    public List<TiposDoc> getAllTiposDocs(){
        return tiposDocRepository.findAll();
    }

    public Parametro getOneParametro(String key){
        return parametroRepository.getOne(key);
    }


    public List<GenericDTO> getAllTiposDocDTO(List<TiposDoc> tiposDocs) {
        List<GenericDTO> tiposDocsDTO = new ArrayList<>();
        for (TiposDoc tiposDoc : tiposDocs) {
            GenericDTO tiposDocDTO = new GenericDTO(tiposDoc.getCodigo(), tiposDoc.getNombre());
            tiposDocsDTO.add(tiposDocDTO);
        }
        return tiposDocsDTO;
    }

    public List<GenericDTO> getAllMediosPagoDTO(List<MedioPago> mediosPagos) {
        List<GenericDTO> mediosPagosDTO = new ArrayList<>();
        for (MedioPago mediosPago : mediosPagos) {
            GenericDTO mediosPagoDTO = new GenericDTO(mediosPago.getCodigo(), mediosPago.getNombre());
            mediosPagosDTO.add(mediosPagoDTO);
        }
        return mediosPagosDTO;
    }

    public List<GenericDTO> getAllBancosDTO(List<Banco> bancos) {
        List<GenericDTO> tiposDocsDTO = new ArrayList<>();
        for (Banco banco : bancos) {
            GenericDTO genericDTO = new GenericDTO(banco.getCodigo(), banco.getNombre());
            tiposDocsDTO.add(genericDTO);
        }
        return tiposDocsDTO;
    }

    public List<EgresoDTO> getAllEgresosDTO(List<Egreso> egresos) {
        List<EgresoDTO> egresosDTO = new ArrayList<>();
        for (Egreso egreso : egresos) {
           EgresoDTO egresoDTO = new EgresoDTO(egreso);
            egresosDTO.add(egresoDTO);
        }
        return egresosDTO;
    }

}
