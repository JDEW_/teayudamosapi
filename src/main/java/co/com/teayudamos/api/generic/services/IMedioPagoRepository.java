package co.com.teayudamos.api.generic.services;

import co.com.teayudamos.api.generic.entity.MedioPago;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IMedioPagoRepository extends JpaRepository<MedioPago, Long> {

}
