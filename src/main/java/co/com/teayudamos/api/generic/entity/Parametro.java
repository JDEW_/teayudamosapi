package co.com.teayudamos.api.generic.entity;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "TAYU_PARAMETROS")
public class Parametro {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @Column(name = "CDPARAMETRO")
    private String codigo;

    @Column(name = "DSNOMBRE", nullable = false, length = 200)
    private String nombre;

    @Column(name = "DSVALOR", nullable = false, length = 255)
    private String valor;

    @Column(name = "DSDESCRIPCION", nullable = false, length = 255)
    private String descripcion;

    @Column(name = "SNACTIVO", nullable = false, length = 1)
    private boolean snActivo;

    @Column(name = "ID_USUARIO_CREACION", nullable = false, length = 15)
    private Long idUsuario;

    @Column(name = "FECREACION", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date fechaCreaion;

    @Column(name = "FEMODIFICACION", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date fechaModificacion;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isSnActivo() {
        return snActivo;
    }

    public void setSnActivo(boolean snActivo) {
        this.snActivo = snActivo;
    }

    public Long getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(Long idUsuario) {
        this.idUsuario = idUsuario;
    }

    public Date getFechaCreaion() {
        return fechaCreaion;
    }

    public void setFechaCreaion(Date fechaCreaion) {
        this.fechaCreaion = fechaCreaion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
}
