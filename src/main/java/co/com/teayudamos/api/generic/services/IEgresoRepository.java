package co.com.teayudamos.api.generic.services;

import co.com.teayudamos.api.generic.entity.Egreso;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IEgresoRepository extends JpaRepository<Egreso, Long> {

}
