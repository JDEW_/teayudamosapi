package co.com.teayudamos.api.generic.services;

import co.com.teayudamos.api.generic.entity.Banco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IBancoRepository extends JpaRepository<Banco, Long> {
}
