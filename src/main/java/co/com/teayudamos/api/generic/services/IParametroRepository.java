package co.com.teayudamos.api.generic.services;

import co.com.teayudamos.api.generic.entity.Parametro;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IParametroRepository extends JpaRepository<Parametro, String> {

}
