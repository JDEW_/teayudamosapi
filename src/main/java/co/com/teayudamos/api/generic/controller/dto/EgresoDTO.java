package co.com.teayudamos.api.generic.controller.dto;

import co.com.teayudamos.api.generic.entity.Egreso;

public class EgresoDTO {

    private String nombre;
    private int porcentaje;

    public EgresoDTO(Egreso egreso) {
        this.nombre = egreso.getNombre();
        this.porcentaje = egreso.getPorcentaje();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(int porcentaje) {
        this.porcentaje = porcentaje;
    }
}
