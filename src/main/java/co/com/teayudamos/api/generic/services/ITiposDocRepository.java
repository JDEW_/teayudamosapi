package co.com.teayudamos.api.generic.services;

import co.com.teayudamos.api.generic.entity.TiposDoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ITiposDocRepository extends JpaRepository<TiposDoc, Long> {

}
