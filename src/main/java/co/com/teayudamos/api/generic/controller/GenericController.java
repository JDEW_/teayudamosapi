package co.com.teayudamos.api.generic.controller;

import co.com.teayudamos.api.generic.controller.dto.GenericDTO;
import co.com.teayudamos.api.generic.entity.Banco;
import co.com.teayudamos.api.generic.entity.MedioPago;
import co.com.teayudamos.api.generic.entity.TiposDoc;
import co.com.teayudamos.api.generic.services.GenericService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin(origins = {"http://localhost:4200", "https://teayudamos-99.firebaseapp.com", "https://teayudamos-all.firebaseapp.com"})
@RestController
@RequestMapping("/generic/v1/")
public class GenericController {

    @Autowired
    private GenericService genericService;

    @GetMapping("/tiposDoc")
    public ResponseEntity<List<GenericDTO>> findAllTiposDoc() {
        List<TiposDoc> tiposDocs = genericService.getAllTiposDocs();
        return ResponseEntity.ok(genericService.getAllTiposDocDTO(tiposDocs));
    }

    @GetMapping("/mediosPago")
    public ResponseEntity<List<GenericDTO>> findAllMediosPago() {
        List<MedioPago> mediosPago = genericService.getAllMedioPagos();
        return ResponseEntity.ok(genericService.getAllMediosPagoDTO(mediosPago));
    }

    @GetMapping("/bancos")
    public ResponseEntity<List<GenericDTO>> findAllBancos() {
        List<Banco> bancos = genericService.getAllBancos();
        return ResponseEntity.ok(genericService.getAllBancosDTO(bancos));
    }
}
