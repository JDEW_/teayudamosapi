package co.com.teayudamos.api.generic.controller.dto;

import co.com.teayudamos.api.generic.entity.TiposDoc;

import java.io.Serializable;

public class GenericDTO implements Serializable {

    private long codigo;
    private String nombre;

    public GenericDTO(long codigo, String nombre) {
        this.codigo = codigo;
        this.nombre = nombre;
    }

    public long getCodigo() {
        return codigo;
    }

    public void setCodigo(long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
