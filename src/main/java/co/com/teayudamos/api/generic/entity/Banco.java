package co.com.teayudamos.api.generic.entity;

import org.springframework.data.annotation.CreatedDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "TAYU_BANCOS")
public class Banco {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long codigo;

    @Column(name = "DSNOMBRE", nullable = false, length = 200)
    private String nombre;

    @Column(name = "SNACTIVO", nullable = false, length = 1)
    private boolean snActivo;

    @Column(name = "FECREACION", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date fechaCreaion;

    public Banco() {
    }

    public Banco(Long codigo) {
        this.codigo = codigo;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isSnActivo() {
        return snActivo;
    }

    public void setSnActivo(boolean snActivo) {
        this.snActivo = snActivo;
    }

    public Date getFechaCreaion() {
        return fechaCreaion;
    }

    public void setFechaCreaion(Date fechaCreaion) {
        this.fechaCreaion = fechaCreaion;
    }
}
