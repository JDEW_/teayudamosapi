package co.com.teayudamos.api.generic.utils;

import java.text.DateFormatSymbols;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class Utils {

    public static Date sumarDiasAFecha(Date fecha, int dias){
        if (dias==0) return fecha;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.DAY_OF_YEAR, dias);
        return calendar.getTime();
    }

    public static Date sumarMesesAFecha(Date fecha, int mes){
        if (mes==0) return fecha;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha);
        calendar.add(Calendar.MONTH, mes);
        return calendar.getTime();
    }

    public static Map<String, String> getMnths(){
        Calendar cal = Calendar.getInstance();
        Map<String, String> map = new HashMap<>();
        map.put("01", "Enero");
        map.put("02", "Febrero");
        map.put("03", "Marzo");
        map.put("04", "Abril");
        map.put("05", "Mayo");
        map.put("06", "Junio");
        map.put("07", "Julio");
        map.put("08", "Agosto");
        map.put("09", "Septiembre");
        map.put("10", "Octubre");
        map.put("11", "Noviembre");
        map.put("12", "Diciembre");
        return map;
    }

}
