package co.com.teayudamos.api.security.controllers;

import java.net.URI;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import co.com.teayudamos.api.exception.EmailException;
import co.com.teayudamos.api.exception.ServiceException;
import co.com.teayudamos.api.security.controllers.dto.JwtAuthenticationResponse;
import co.com.teayudamos.api.security.controllers.dto.LoginRequest;
import co.com.teayudamos.api.security.controllers.dto.SignUpRequest;
import co.com.teayudamos.api.security.entity.User;
import co.com.teayudamos.api.security.services.UserService;
import co.com.teayudamos.api.security.utils.JwtTokenProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;


@RestController
@RequestMapping("/security/auth/v1/")
@CrossOrigin(origins = {"http://localhost:4200", "https://teayudamos-99.firebaseapp.com", "https://teayudamos-all.firebaseapp.com"})
@Api("Endpoints de autenticacion al sistema.")
public class AuthController {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserService userService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider tokenProvider;

	@RequestMapping(value = "/signin", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "REST para autenticar al usuario en el sistema", response = JwtAuthenticationResponse.class)
	public ResponseEntity<?> authenticateUser(
			@ApiParam("Datos del login.") @Valid @RequestBody LoginRequest loginRequest) {

		Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);

		String jwt = tokenProvider.generateToken(authentication, false);
		String jwtRefresh = tokenProvider.generateToken(authentication, true);
		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, jwtRefresh));
	}

	
	@RequestMapping(value = "/signup", method = RequestMethod.POST, produces = "application/json")
	@ApiOperation(value = "REST para almacenar la informacion del usuario.", response = URI.class)
	public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpRequest signUpRequest) throws ServiceException {
		URI location = null;
		try {
			User user = new User(signUpRequest.getNombre(), signUpRequest.getEmail(), signUpRequest.getPassword());

			user.setPassword(passwordEncoder.encode(user.getPassword()));

			User result = userService.saveUsuario(user);

			location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{username}")
					.buildAndExpand(result.getEmail()).toUri();
		} catch (EmailException e) {
			return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
		}

		return ResponseEntity.created(location).body("User registered successfully");
	}
}
