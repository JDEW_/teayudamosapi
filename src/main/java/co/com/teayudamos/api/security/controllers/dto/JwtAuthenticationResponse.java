package co.com.teayudamos.api.security.controllers.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Clase que representa la informacion del token de seguridad del aplicativo.")
public class JwtAuthenticationResponse {
	
	@ApiModelProperty(notes = "Token de acceso al sistema.")
	private String accessToken;
	
	@ApiModelProperty(notes = "Token de refresh al token actual para acceder al sistema.")
	private String refreshToken;
	
	@ApiModelProperty(notes = "Tipo de token.")
    private String tokenType = "Bearer";

    public JwtAuthenticationResponse(String accessToken, String refreshToken) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
    }
    
    public JwtAuthenticationResponse(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public void setTokenType(String tokenType) {
        this.tokenType = tokenType;
    }

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}
}
