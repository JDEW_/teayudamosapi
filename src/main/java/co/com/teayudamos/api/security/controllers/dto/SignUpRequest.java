package co.com.teayudamos.api.security.controllers.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Clase que representa la informacion del usuario para se ingresado en el sistema.")
public class SignUpRequest {
	@NotBlank
	@Size(min = 4, max = 60)
	@ApiModelProperty(notes = "Nombre de registro en el sistema.")
	private String nombre;

	@NotBlank
	@Size(max = 60)
	@Email
	@ApiModelProperty(notes = "Email de registro en el sistema.")
	private String email;

	@NotBlank
	@Size(min = 6, max = 20)
	@ApiModelProperty(notes = "Password de registro en el sistema.")
	private String password;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
