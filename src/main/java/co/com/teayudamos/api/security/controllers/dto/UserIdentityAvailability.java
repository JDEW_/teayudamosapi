package co.com.teayudamos.api.security.controllers.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Clase que representa la informacion acerca de la disponibillidad de un Email.")
public class UserIdentityAvailability {
	
	@ApiModelProperty(notes = "Indica si un Email esta disponible.", example = "true/false")
    private Boolean available;

    public UserIdentityAvailability(Boolean available) {
        this.available = available;
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }
}