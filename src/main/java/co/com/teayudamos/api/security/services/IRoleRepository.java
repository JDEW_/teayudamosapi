package co.com.teayudamos.api.security.services;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.teayudamos.api.security.dto.RoleName;
import co.com.teayudamos.api.security.entity.Role;

@Repository
public interface IRoleRepository extends JpaRepository<Role, Long> {
	public Optional<Role> findByName(RoleName roleName);
}
