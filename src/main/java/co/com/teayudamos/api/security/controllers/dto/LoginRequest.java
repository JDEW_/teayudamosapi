package co.com.teayudamos.api.security.controllers.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Clase que representa la informacion del usuario para acceder al sistema.")
public class LoginRequest {
	
	@NotBlank
	@Size(min = 1, max = 100)
    @ApiModelProperty(notes = "Email de acceso al sistema.", example = "xxx@y.com")
	private String email;

	@NotBlank
	@Size(min = 6, max = 50)
	@ApiModelProperty(notes = "Contrasena de acceso al sistema.", example = "tec!43!990")
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}