package co.com.teayudamos.api.security.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import co.com.teayudamos.api.security.controllers.dto.Menu;
import co.com.teayudamos.api.security.controllers.dto.UserIdentityAvailability;
import co.com.teayudamos.api.security.controllers.dto.UserSummary;
import co.com.teayudamos.api.security.dto.ICurrentUser;
import co.com.teayudamos.api.security.dto.UserPrincipal;
import co.com.teayudamos.api.security.services.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@CrossOrigin(origins = {"http://localhost:4200", "https://teayudamos-99.firebaseapp.com", "https://teayudamos-all.firebaseapp.com"})
@RequestMapping("/security/v1/")
@Api("Endpoints para entregar la informacion del usuario.")
public class UserController {

    @Autowired
    private UserService userService;

    @PreAuthorize("hasRole('ROLE_USER')")
    @RequestMapping(value = "/user/me", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "REST para solicitar la informacion de usuario logueado.", response = UserSummary.class)
    public UserSummary getCurrentUser(@ICurrentUser UserPrincipal currentUser) {
        UserSummary userSummary = new UserSummary(currentUser.getId(), currentUser.getEmail(), currentUser.getName());
        List<Menu> opciones = new ArrayList<Menu>();
        opciones.add(new Menu("fa-user", "Perfil", "/#!/perfil"));
        opciones.add(new Menu("fa-shield", "Club", "/#!/club"));
        userSummary.setOpciones(opciones);
        
        return userSummary;
    }

    @RequestMapping(value = "/user/checkEmailAvailability", method = RequestMethod.GET, produces = "application/json")
    @ApiOperation(value = "REST para validar si hay un email disponible.", response = UserIdentityAvailability.class)
    public UserIdentityAvailability checkEmailAvailability(@RequestParam(value = "email") String email) {
        Boolean isAvailable = !userService.existsByEmail(email);
        return new UserIdentityAvailability(isAvailable);
    }

}