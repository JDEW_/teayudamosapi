package co.com.teayudamos.api.security.services;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.teayudamos.api.security.entity.User;

public interface IUserRepository extends JpaRepository<User, Long> {
	
	public Optional<User> findByEmail(String email);

	public List<User> findByIdIn(List<Long> userIds);

	public Boolean existsByEmail(String email);
}
