package co.com.teayudamos.api.security.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.teayudamos.api.exception.ServiceException;
import co.com.teayudamos.api.generic.utils.Messages;
import co.com.teayudamos.api.security.dto.RoleName;
import co.com.teayudamos.api.security.entity.Role;

/**
 * Servicio que controla la informacion de los roles del sistema
 * @author edwaveor
 *
 */
@Service
public class RoleServices {

	@Autowired
	private IRoleRepository roleRepository;

	@Autowired
	private Messages messages;

	/**
	 * Metodo que busca ls roles por el nombre
	 * @param roleName
	 * @return
	 * @throws ServiceException
	 */
	public Role findByName(RoleName roleName) throws ServiceException {
		return roleRepository.findByName(roleName).orElseThrow(
				() -> new ServiceException(messages.getMessages("roleServices.findByName.sinRole", new String[] {roleName.name()}), ServiceException.CODIGO_EXCEPTION_GENERAL));
	}
}
