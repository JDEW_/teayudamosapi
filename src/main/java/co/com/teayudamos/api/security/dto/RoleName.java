package co.com.teayudamos.api.security.dto;

public enum RoleName {
	ROLE_USER,
    ROLE_ADMIN
}
