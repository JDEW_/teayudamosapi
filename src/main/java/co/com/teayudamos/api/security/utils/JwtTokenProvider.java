package co.com.teayudamos.api.security.utils;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import co.com.teayudamos.api.generic.utils.Messages;
import co.com.teayudamos.api.security.dto.UserPrincipal;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;

@Component
public class JwtTokenProvider {
    private static final Logger logger = LoggerFactory.getLogger(JwtTokenProvider.class);

    @Value("${app.jwtSecret}")
    private String jwtSecret;

    @Value("${app.jwtExpirationInMs}")
    private int jwtExpirationInMs;
    
    
    @Value("${app.jwtRefreshExpirationInMs}")
    private int jwtRefreshExpirationInMs;
    
    @Autowired
  	private Messages messages;

    public String generateToken(Authentication authentication, boolean isRefresh) {

        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();

        int jwtTime = (isRefresh ? jwtRefreshExpirationInMs : jwtExpirationInMs);
        
        Date now = new Date();
        Date expiryDate = new Date(now.getTime() + jwtTime);

        return Jwts.builder()
                .setSubject(Long.toString(userPrincipal.getId()))
                .setIssuedAt(new Date())
                .setExpiration(expiryDate)
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    public Long getUserIdFromJWT(String token) {
        Claims claims = Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();

        return Long.parseLong(claims.getSubject());
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException ex) {
            logger.error(messages.getMessages("jwtTokenProvider.signatureException"));
        } catch (MalformedJwtException ex) {
            logger.error(messages.getMessages("jwtTokenProvider.malformedJwtException"));
        } catch (ExpiredJwtException ex) {
            logger.error(messages.getMessages("jwtTokenProvider.expiredJwtException"));
        } catch (UnsupportedJwtException ex) {
            logger.error(messages.getMessages("jwtTokenProvider.unsupportedJwtException"));
        } catch (IllegalArgumentException ex) {
            logger.error(messages.getMessages("jwtTokenProvider.illegalArgumentException"));
        }
        return false;
    }
}
