package co.com.teayudamos.api.security.controllers.dto;

import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Clase que representa la informacion del menu para acceder al sistema.")
public class Menu {

    @ApiModelProperty(notes = "Codigo del recurso de menu.")
	private String codigo;
	
    @ApiModelProperty(notes = "Nombre del icono para visualizar el menu.")
	private String icono;
	
    @ApiModelProperty(notes = "Nombre de visualizacion del menu.")
	private String nombre;
	
    @ApiModelProperty(notes = "Url donde redirecciona la opcion del menu.")
	private String url;
	
	public Menu(String icono, String nombre, String url) {
		this.icono = icono;
		this.nombre = nombre;
		this.url = url;
	}

	public String getCodigo() {
		return codigo;
	}

	public String getIcono() {
		return icono;
	}

	public String getNombre() {
		return nombre;
	}

	public String getUrl() {
		return url;
	}

}
