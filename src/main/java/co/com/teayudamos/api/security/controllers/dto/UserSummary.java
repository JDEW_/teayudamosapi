package co.com.teayudamos.api.security.controllers.dto;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Clase que representa la informacion del usuario que ingreso en el sistema.")
public class UserSummary {
	
	@ApiModelProperty(notes = "Codigo del usuario en el sistema.", example = "1")
    private Long codigo;
	
	@ApiModelProperty(notes = "Email de acceso al sistema.", example = "xxx@y.com")
    private String email;
	
	@ApiModelProperty(notes = "Nombre del usuario en el sistema.", example = "Juan Perez")
    private String nombre;
	
	@ApiModelProperty(notes = "Opciones que tiene acceso el usuario en el sistema.")
    private List<Menu> opciones;

    public UserSummary(Long codigo, String email, String nombre) {
        this.codigo = codigo;
        this.email = email;
        this.nombre = nombre;
    }

    public Long getCodigo() {
        return codigo;
    }

    public void setCodigo(Long codigo) {
        this.codigo = codigo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

	public List<Menu> getOpciones() {
		return opciones;
	}

	public void setOpciones(List<Menu> opciones) {
		this.opciones = opciones;
	}
}