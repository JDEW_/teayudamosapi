package co.com.teayudamos.api.security.services;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.teayudamos.api.exception.EmailException;
import co.com.teayudamos.api.exception.ResourceNotFoundException;
import co.com.teayudamos.api.exception.ServiceException;
import co.com.teayudamos.api.generic.utils.Messages;
import co.com.teayudamos.api.security.dto.RoleName;
import co.com.teayudamos.api.security.dto.UserPrincipal;
import co.com.teayudamos.api.security.entity.Role;
import co.com.teayudamos.api.security.entity.User;

/**
 * Servicio para el manejo de la informacion del usuario en el sistema.
 * 
 * @author edwaveor
 *
 */
@Service
public class UserService implements UserDetailsService {

	@Autowired
	private IUserRepository userRepository;

	@Autowired
	private RoleServices roleServices;

	@Autowired
	private Messages messages;

	/**
	 * Metodo que carga la informacion del usuario por el email.
	 */
	@Override
	@Transactional
	public UserDetails loadUserByUsername(String email) {
		User user = userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException(messages
				.getMessages("userService.loadUserByUsername.usuarioNoExiste", new String[] { email })));

		return UserPrincipal.create(user);
	}

	/**
	 * Metodo que carga la informacion del usuario por el ID.
	 * 
	 * @param id
	 * @return
	 */
	@Transactional
	public UserDetails loadUserById(Long id) {
		User user = userRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("User", "id", id));

		return UserPrincipal.create(user);
	}

	/**
	 * Metodo que cidentifica si existe un email en el sistema.
	 * 
	 * @param email
	 * @return
	 */
	@Transactional
	public boolean existsByEmail(String email) {
		return userRepository.existsByEmail(email);
	}

	/**
	 * Metodo que almacena la informacion del usuario.
	 * 
	 * @param user
	 * @return
	 * @throws ServiceException
	 */
	@Transactional
	public User saveUsuario(User user) throws ServiceException, EmailException {
		User userReturn = null;
		if (!existsByEmail(user.getEmail())) {
			Role userRole = roleServices.findByName(RoleName.ROLE_USER);
			user.setRoles(Collections.singleton(userRole));

			userReturn = userRepository.save(user);
		} else {
			new EmailException(messages.getMessages("userService.saveUsuario.usuarioExiste", new String [] {user.getEmail()}));
		}

		return userReturn;
	}
}
