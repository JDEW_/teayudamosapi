--ALTER USER 'teayudamos'@'localhost' IDENTIFIED WITH mysql_native_password BY 'pass123';

-- Creacion de bancos
INSERT INTO tayu_bancos (id, fecreacion, dsnombre, snactivo)
VALUES
	(1, '2019-03-28 13:02:50', 'BANCOLOMBIA', 1);

-- Creacion de medios de pago
INSERT INTO tayu_medios_pagos (id, fecreacion, dsnombre, snactivo)
VALUES
	(1, '2019-04-10 12:33:46', 'EFECTIVO', 1);

-- Creacion de socios
INSERT INTO tayu_socios (id, dscedula, fecreacion, femodificacion, dsnombre, snactivo, id_usuario_creacion, id_cdtipo_doc)
VALUES
	(1, '2323232', '2019-02-26 00:00:00', NULL, 'EDWIN', 1, 1, 1);

-- Creacion de tipos de documento
INSERT INTO tayu_tipos_doc (id, fecreacion, dsnombre, snactivo)
VALUES
	(1, '2019-02-26 00:00:00', 'CEDULA', 1);


-- Creacion de roles
INSERT INTO tseg_roles (id, name)
VALUES
	(1, 'ROLE_USER'),
	(2, 'ROLE_ADMIN');

-- Creacion de usuarios
INSERT INTO tseg_usuarios (id, created_at, updated_at, email, name, password)
VALUES
	(1, '2019-03-28 13:00:33', '2019-03-28 13:00:33', 'edvelas@gmail.com', 'Edward V', '$2a$10$k0TVSmq87LRGTmwon2bUkehiSJxkxviDsjbGT5nW3Nnqb8gvetSUK');


-- Creacion de roles por usuario
INSERT INTO tseg_user_roles (user_id, role_id)
VALUES
	(2, 1);


